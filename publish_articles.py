#!/usr/bin/python
# -*- coding: utf-8 -*-

from familywealth.models import Article
import datetime


now = datetime.datetime.now()
for a in Article.objects.filter(published=False, publication='P'):
    if a.publish_date <= now and not a.unpublish_date:
        a.published = True
        a.creation_date = a.publish_date
        a.save()
    elif a.publish_date <= now and a.unpublish_date >= now:
        a.published = True
        a.creation_date = a.publish_date
        a.save()

for a in Article.objects.filter(published=True, publication='P'):
    try:
        if a.unpublish_date <= now:
            a.published = False
            a.save()
    except TypeError:
        pass

