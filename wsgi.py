import os, sys
import site

PROJECT_PATH = os.path.realpath(os.path.dirname(__file__))
FOLDER_PATH = os.path.realpath(os.path.dirname(__file__)+"/../")
sys.path.append(PROJECT_PATH)
sys.path.append(FOLDER_PATH)

os.environ["DJANGO_SETTINGS_MODULE"] = "settings"

import django.core.handlers.wsgi
application = django.core.handlers.wsgi.WSGIHandler()
