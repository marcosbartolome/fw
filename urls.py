from django.conf.urls.defaults import *
from django.conf import settings
from django.contrib import admin

admin.autodiscover()

urlpatterns = patterns('',
    # Static content
    url(r'^%s(?P<path>.*)$' % settings.MEDIA_URL.lstrip('/'), 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
    
    # Admin
    url(r'^admin/inform/$', 'familywealth.views.inform', name="inform"),
    (r'^admin/', include(admin.site.urls)),    

    #Profile
    url(r'^profile/$', 'familywealth.views.profile_favourites', name="profile-favourites"),
    url(r'^profile/valorations/$', 'familywealth.views.profile_valorations', name="profile-valorations"),
    url(r'^profile/discussions/$', 'familywealth.views.profile_discussions', name="profile-discussions"),
    url(r'^profile/events/$', 'familywealth.views.profile_events', name="profile-events"),
    url(r'^profile/comments/$', 'familywealth.views.profile_comments', name="profile-comments"),
    url(r'^profile/notifications/$', 'familywealth.views.profile_notifications', name="profile-notifications"),
    url(r'^profile/popup/basic/$', 'familywealth.views.popup_profile', name="popup-profile"),
    url(r'^profile/popup/avatar/$', 'familywealth.views.popup_avatar', name="popup-avatar"),
    url(r'^profile/popup/avatar/set/$', 'familywealth.views.set_avatar', name="set-avatar"),
    #Profile ajax deletion
    url(r'^profile/ajax/delete/favourite/(?P<id>\d+)/$', 'familywealth.views.delete_favourite', name="delete-favourite"),
    url(r'^profile/ajax/delete/valoration/(?P<id>\d+)/$', 'familywealth.views.delete_valoration', name="delete-valoration"),
    url(r'^profile/ajax/delete/comment/(?P<id>\d+)/$', 'familywealth.views.delete_comment', name="delete-comment"),
    url(r'^profile/ajax/delete/notification/(?P<id>\d+)/$', 'familywealth.views.delete_notification', name="delete-notification"),
    
    # Home
    url(r'^$', 'familywealth.views.home', name="home"),
    
    # Article
    url(r'^content/pdf/(?P<slug>[\w\d\-]+)/$', 'familywealth.views.content_pdf', name="content-pdf"),
    url(r'^content/link/(?P<slug>[\w\d\-]+)/$', 'familywealth.views.content_link', name="content-link"),
    url(r'^content/video/(?P<slug>[\w\d\-]+)/$', 'familywealth.views.content_video', name="content-video"),
    url(r'^content/(?P<slug>[\w\d\-]+)/$', 'familywealth.views.content_pdf', name="content"),    
    url(r'^content/ajax/valoration/(?P<slug>[\w\d\-]+)/(?P<valoration>[1|2|3|4|5])/$', 'familywealth.views.ajax_valoration', name="ajax-valoration"),

    # Favourites (add/delete)
    url(r'^favourites/add/(?P<slug>[\w\d\-]+)/$', 'familywealth.views.add_favourite', name="add-favourite"),
    url(r'^favourites/del/(?P<slug>[\w\d\-]+)/$', 'familywealth.views.del_favourite', name="del-favourite"),
    
    # Comments
    url(r'^comment/add/(?P<slug>[\w\d\-]+)/(?P<parent_comment_id>\d+)/$', 'familywealth.views.add_comment', name="add-comment"),
    url(r'^comment/add/(?P<slug>[\w\d\-]+)/$', 'familywealth.views.add_comment', name="add-comment"),
    
    #Categories
    url(r'^category/(?P<slug>[\w\d\-]+)/$', 'familywealth.views.category', name="category"),
    
    
    #Discussion
    url(r'^discussions/$', 'familywealth.views.discussions', name="discussions"),
    url(r'^discussion/(?P<slug>[\w\d\-]+)/$', 'familywealth.views.discussion', name="discussion"),
    
    #Event
    url(r'^events/$', 'familywealth.views.events', name="events"),
    url(r'^event/(?P<slug>[\w\d\-]+)/$', 'familywealth.views.event', name="event"),
    
    #Ajax views for infinite listings:
    url(r'^ajax/article/list/$', 'familywealth.views.ajax_list_articles', name="ajax-list-articles"),
    url(r'^ajax/event/list/$', 'familywealth.views.ajax_list_events', name="ajax-list-events"),
    url(r'^ajax/discussion/$', 'familywealth.views.ajax_list_discussions', name="ajax-list-discussions"),
    url(r'^ajax/category/(?P<slug>[\w\d\-]+)/$', 'familywealth.views.ajax_list_categories', name="ajax-list-categories"),
    
    #Whoweare
    url(r'^about_us/$', 'familywealth.views.whoweare', name="whoweare"),

    #Speak
    url(r'^speak/$', 'familywealth.views.speak', name="speak"),

    #Static content
    url(r'^contact/$', 'familywealth.views.contact', name="contact"),    
    url(r'^about/$', 'familywealth.views.about', name="about"),    
    url(r'^legal/$', 'familywealth.views.legal', name="legal"),    
    url(r'^conditions/$', 'familywealth.views.conditions', name="conditions"),    
    
    #Search
    url(r'^tag/news/(?P<tag_slug>[\w\d\-]+)/$', 'familywealth.views.search_articles_tag', name="search-articles-tag"),    
    url(r'^search/news/$', 'familywealth.views.search_articles', name="search-articles"),    
    url(r'^tag/discussions/(?P<tag_slug>[\w\d\-]+)/$', 'familywealth.views.search_discussions_tag', name="search-discussions-tag"),    
    url(r'^search/discussions/$', 'familywealth.views.search_discussions', name="search-discussions"),    
    url(r'^tag/events/(?P<tag_slug>[\w\d\-]+)/$', 'familywealth.views.search_events_tag', name="search-events-tag"),    
    url(r'^search/events/$', 'familywealth.views.search_events', name="search-events"),    
    url(r'^tag/comments/(?P<tag_slug>[\w\d\-]+)/$', 'familywealth.views.search_comments_tag', name="search-comments-tag"),    
    url(r'^search/comments/$', 'familywealth.views.search_comments', name="search-comments"),    
    url(r'^tag/(?P<tag_slug>[\w\d\-]+)/$', 'familywealth.views.search_tag', name="search-tag"),    
    url(r'^search/$', 'familywealth.views.search', name="search"),    

    #Auth    
    url(r'^login/$', 'familywealth.views.login_fw', name="login"),    
    url(r'^logout/$', 'familywealth.views.logout_fw', name="logout"),    
    url(r'^password_reset/$', 'familywealth.views.password_reset_fw', name="password_reset"),    
    url(r'^password_reset_ok/$', 'familywealth.views.password_reset_ok', name="password_reset_ok"),    
    url(r'^password_reset_confirm/(?P<uidb36>[\w\d\-]+)/(?P<token>[\w\d\-]+)/$', 'familywealth.views.password_reset_confirm_fw', name="password_reset_confirm"),    
    url(r'^password_reset_complete/$', 'familywealth.views.password_reset_complete_fw', name="password_reset_complete"),    

    #Registration
    url(r'^register/$', 'familywealth.views.register', name="register"),    
    url(r'^create_user/$', 'familywealth.views.create_user', name="create-user"),    
    
    #Newsletter
    url(r'^newsletter/(?P<slug>[\w\d\-]+)/$', 'familywealth.views.newsletter', name="newsletter"),    

    #Informe last login
    url(r'^informe_last_login/$', 'familywealth.views.last_login', name="last_login"),    
    url(r'^download/(?P<filename>\d\d-\d\d-\d\d\d\d.html)/$', 'familywealth.views.download', name="download"),
)
