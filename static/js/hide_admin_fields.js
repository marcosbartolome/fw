function on_change_type(eventObj){
        element_type = django.jQuery(eventObj.target)
        type_value = element_type.val();
        if (type_value == "") {
            element_type.parent().parent().parent().find('.file').hide();
            element_type.parent().parent().parent().find('.link').hide();
            element_type.parent().parent().parent().find('.video').hide();
        } else if (type_value == "0") {
            element_type.parent().parent().parent().find('.file').show();
            element_type.parent().parent().parent().find('.file label').addClass('required');
            element_type.parent().parent().parent().find('.link').hide();
            element_type.parent().parent().parent().find('.video').hide();
        } else if (type_value == "1") {
            element_type.parent().parent().parent().find('.file').hide();
            element_type.parent().parent().parent().find('.link').show();
            element_type.parent().parent().parent().find('.link label').addClass('required');
            element_type.parent().parent().parent().find('.video').hide();
        } else if (type_value == "2") {
            element_type.parent().parent().parent().find('.file').hide();
            element_type.parent().parent().parent().find('.link').hide();
            element_type.parent().parent().parent().find('.video').show();
            element_type.parent().parent().parent().find('.video label').addClass('required');
        }
}

django.jQuery(document).ready(function() {
    django.jQuery('#articles-group>div .type select').each( function(index,element) {
	    django.jQuery(element).change(on_change_type);
        django.jQuery(element).trigger('change');
    });
    django.jQuery('#article_form .type select').change(on_change_type);
    django.jQuery('#article_form .type select').trigger('change');
});

