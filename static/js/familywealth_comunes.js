$(document).ready(function() {


	var timeout;
	//SUBMEN�
	$(".submenu_parent").each(function(){
		$(this).parent().mouseenter(function(){
			clearTimeout(timeout);
			$('.submenu').fadeOut();
			$('.submenu_parent').parent().removeClass('abierto');
			$(this).find('.submenu').fadeIn();
			$(this).toggleClass('abierto');
		})
		//SUBMEN�
		$(this).parent().mouseleave(function(){
			timeout = setTimeout(function(){
				$('.submenu').fadeOut();
				$(".submenu_parent").parent().removeClass('abierto');
			},500);
		})
		$(this).find('.submenu').mouseenter(function(){
			clearTimeout(timeout);
		});
		$(this).find('.submenu').mouseleave(function(){
			timeout = setTimeout(function(){
				$('.submenu').fadeOut();
				$(".submenu_parent").parent().removeClass('abierto');
			},500);
		});
		//pantalla t�ctil
		$(this).click(function(){
			$(this).parent().find('.submenu').toggle('show');
			$(this).parent().toggleClass('abierto');
		});
	});

	
	//RESUMEN (home)
	$('.resumen_detalle').hide();
	$('.resumen').toggleClass('abierto');
	$('.res_abrir').click(function(e){
		$('.resumen_detalle').slideToggle();
		$('.resumen').toggleClass('abierto');
		if($('.resumen').hasClass('abierto')){
			$('.res_abrir').html('Cerrar');
		}else{
			$('.res_abrir').html('Abrir');
		}
		e.preventDefault();
	});
	
	//botones
	$('.boton').mouseenter(function(e){
		if(!$(this).hasClass('activo')){
			$(this).toggleClass('hover');
		}
	});
	$('.boton').mouseleave(function(e){
		if(!$(this).hasClass('activo')){
			$(this).toggleClass('hover');
		}
	});
	(function( $ ) {
		$.widget( "ui.combobox", {
			_create: function() {
				var self = this,
					select = this.element.hide(),
					selected = select.children( ":selected" ),
					value = selected.val() ? selected.text() : "";
				var input = this.input = $( "<input>" )
					.insertAfter( select )
					.val( value )
					.autocomplete({
						delay: 0,
						minLength: 0,
						source: function( request, response ) {
							var matcher = new RegExp( $.ui.autocomplete.escapeRegex(request.term), "i" );
							response( select.children( "option" ).map(function() {
								var text = $( this ).text();
								if ( this.value && ( !request.term || matcher.test(text) ) )
									return {
										label: text.replace(
											new RegExp(
												"(?![^&;]+;)(?!<[^<>]*)(" +
												$.ui.autocomplete.escapeRegex(request.term) +
												")(?![^<>]*>)(?![^&;]+;)", "gi"
											), "<strong>$1</strong>" ),
										value: text,
										option: this
									};
							}) );
						},
						select: function( event, ui ) {
							ui.item.option.selected = true;
							self._trigger( "selected", event, {
								item: ui.item.option
							});
						},
						change: function( event, ui ) {
							if ( !ui.item ) {
								var matcher = new RegExp( "^" + $.ui.autocomplete.escapeRegex( $(this).val() ) + "$", "i" ),
									valid = false;
								select.children( "option" ).each(function() {
									if ( $( this ).text().match( matcher ) ) {
										this.selected = valid = true;
										return false;
									}
								});
								if ( !valid ) {
									// remove invalid value, as it didn't match anything
									$( this ).val( "" );
									select.val( "" );
									input.data( "autocomplete" ).term = "";
									return false;
								}
							}
						}
					})
					.addClass( "ui-widget ui-widget-content ui-corner-left" );

				input.data( "autocomplete" )._renderItem = function( ul, item ) {
						
					return $( "<li></li>" )
						.data( "item.autocomplete", item )
						.append( "<a>" + item.label + "</a>" )
						.appendTo( ul );
				}

				this.button = $( "<button type='button'>&nbsp;</button>" )
					.attr( "tabIndex", -1 )
					.attr( "title", "Show All Items" )
					.insertAfter( input )
					.button({
						icons: {
							primary: "ui-icon-triangle-1-s"
						},
						text: false
					})
					.removeClass( "ui-corner-all" )
					.addClass( "ui-corner-right ui-button-icon" )
					.click(function() {
						// close if already visible
						if (input.autocomplete( "widget" ).is( ":visible") ) {
							input.autocomplete( "close" );
							return;
						}

						// pass empty string as value to search for, displaying all results
						input.autocomplete( "search", "" );
						input.focus();
						//lo subimos 3px para que pise el borde del input y parezca que no hay
						$('.ui-autocomplete').css('top',parseInt($('.ui-autocomplete').css('top').replace('px',''))-3);
						//lo hacemos crecer 25px para que contin�e hasta el bot�n
						$('.ui-autocomplete').css('width',parseInt($('.ui-autocomplete').css('width').replace('px',''))+26);
					});
			},

			destroy: function() {
				this.input.remove();
				this.button.remove();
				this.element.show();
				$.Widget.prototype.destroy.call( this );
			}
		});
	})( jQuery );



	$( ".combobox" ).combobox();

	//funcionalidad para sustituir el valor por defecto
	var valorTxt = [];
	$(".borracontenido").each(function(index){
		valorTxt.push($(this).val());
		$(this).focus(function () { 
			if($(this).val()==valorTxt[index]) $(this).val('');
		});
		
		$(this).blur(function () { 
			if($(this).val()=='') $(this).val(valorTxt[index]);
		});
	});
	//funcionalidad para sustituir el valor por defecto en buscador
	var valorBuscador = '';
		valorBuscador=$('.input_buscar').val();
		$('.input_buscar').focus(function () { 
			if($('.input_buscar').val()==valorBuscador) $('.input_buscar').val('');
		});
		
		$('.input_buscar').blur(function () { 
			if($('.input_buscar').val()=='') $('.input_buscar').val(valorBuscador);
		});
	/* 
	$('.perfil_modal').dialog({
		autoOpen: false,
		modal: true,
		draggable: false,
		closeText: 'Cerrar',
		resizable: false
	}); */
	
	//EDITAR PERFIL
	$(".editarperfil").colorbox({
		width:"360px", height:"500px", iframe:true, transition:"elastic",speed:"650",
		close: "Cerrar" //IMPORTANTE, ESTO CAMBIAR EN MULTILENGUAJE!!!!
		
	});

	//EDITAR AVATAR
	$(".perfil").colorbox({
		width:"400px", height:"90%", iframe:true, transition:"elastic",speed:"650",
		close: "Cerrar" //IMPORTANTE, ESTO CAMBIAR EN MULTILENGUAJE!!!!
		
	});
    
	//ART�CULOS ANTERIORES
	$('.articulos_anteriores').click(function(e){
		var nuevoContenido = $('<div class="contenedorajax"></div>');
		nuevoContenido.load($(this).attr('href'), function() {
			nuevoContenido.appendTo($('.listado_breves:last')).hide().slideDown();
		});
		e.preventDefault();
	});
    
	//CATEGORIA ANTERIORES
	$('.categoria_anteriores').click(function(e){
		var nuevoContenido = $('<div class="contenedorajax"></div>');
		nuevoContenido.load($(this).attr('href'), function() {
			nuevoContenido.appendTo($('.eventos:last')).hide().slideDown();
		});
		e.preventDefault();
	});
        
    
	//DEBATES ANTERIORES
	$('.debates_anteriores').click(function(e){
		var nuevoContenido = $('<div class="contenedorajax"></div>');
		nuevoContenido.load($(this).attr('href'), function() {
			nuevoContenido.appendTo($('.modulo:last')).hide().slideDown();
		});
		e.preventDefault();
	});
	
	//EVENTOS ANTERIORES
	$('.eventos_anteriores').click(function(e){
		var nuevoContenido = $('<div class="contenedorajax"></div>');
		nuevoContenido.load($(this).attr('href'), function() {
			nuevoContenido.appendTo($('.eventos_pasados:last')).hide().slideDown();
		});
		e.preventDefault();
	});
	
	//PERFIL
	/*
     * $('.perfil').click(function(e){
		$(".perfil_avatar").dialog('open');
		$(".perfil_avatar").dialog('option','width',500);
		$(".perfil_avatar").dialog('option','height',460);
		
		$(".perfil_avatar").find('.cerrar').click(function(e){
			$(".perfil_avatar").dialog('close');
			e.preventDefault();
		});
		e.preventDefault();
	});
    * */
    
	//EDITAR PERFIL
	/* $('.editarperfil').click(function(e){
		$(".perfil_actualizarperfil").dialog('open');
		$(".perfil_actualizarperfil").dialog('option','width',405);
		$(".perfil_actualizarperfil").dialog('option','height',520);
		
		$(".perfil_actualizarperfil").find('.cerrar').click(function(e){
			$(".perfil_actualizarperfil").dialog('close');
			e.preventDefault();
		});
		e.preventDefault();
	}); */
	
	//PESTA�ERO
	$('.pestanero').tabs();
	
	//ELIMINAR FAVORITO
	$('.eliminar').find('a').click(function(e){
        $(this).load($(this).attr('href'));
		$(this).parent().parent().slideUp(function(){
            console.log($(this).attr('href'));			
		});
		e.preventDefault();
	});
	
	//RESPONDER MENSAJE
	$('.comentarios_anidados').find('.comentario_escribir').hide();
	$('.respondermensaje').each(function(){
		$(this).click(function(e){
			$(this).parents('.comentario_cont').next().find('.comentario_escribir').slideToggle();
			e.preventDefault();
		});
	});
	
	//ORIENTACION EN IPAD:
	window.onorientationchange = function() {
	  var orientation = window.orientation;
	  switch(orientation) {
		case 0:
			document.body.setAttribute("class","portrait");
			$('#colorbox').removeClass('cboxhorizontal');
			$('#cboxWrapper').removeClass('cboxhorizontal');
			$('#cboxContent').removeClass('cboxhorizontal');
			$('#cboxLoadedContent').removeClass('cboxhorizontal');
			$('#colorbox').addClass('cboxvertical');
			$('#cboxWrapper').addClass('cboxvertical');
			$('#cboxContent').addClass('cboxvertical');
			$('#cboxLoadedContent').addClass('cboxvertical');
			$('#colorbox').css('height','400px');
			$('#cboxWrapper').css('height','400px');
			$('#cboxContent').css('height','400px');
			break; 
		case 90:
			document.body.setAttribute("class","landscape");
			$('#colorbox').removeClass('cboxvertical');
			$('#cboxWrapper').removeClass('cboxvertical');
			$('#cboxContent').removeClass('cboxvertical');
			$('#cboxLoadedContent').removeClass('cboxvertical');
			$('#colorbox').addClass('cboxhorizontal');
			$('#cboxWrapper').addClass('cboxhorizontal');
			$('#cboxContent').addClass('cboxhorizontal');
			$('#cboxLoadedContent').addClass('cboxhorizontal');
			$('#colorbox').css('height','200px');
			$('#cboxWrapper').css('height','200px');
			$('#cboxContent').css('height','200px');
			break;
		case -90: 
			document.body.setAttribute("class","landscape");
			$('#colorbox').removeClass('cboxvertical');
			$('#cboxWrapper').removeClass('cboxvertical');
			$('#cboxContent').removeClass('cboxvertical');
			$('#cboxLoadedContent').removeClass('cboxvertical');
			$('#colorbox').addClass('cboxhorizontal');
			$('#cboxWrapper').addClass('cboxhorizontal');
			$('#cboxContent').addClass('cboxhorizontal');
			$('#cboxLoadedContent').addClass('cboxhorizontal');
			$('#colorbox').css('height','200px');
			$('#cboxWrapper').css('height','200px');
			$('#cboxContent').css('height','200px');
			break;
	  }
	}
});
