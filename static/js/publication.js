jQuery = django.jQuery;
jQuery(document).ready(function() {
    if (jQuery('#id_publication_0').attr('checked')) {

        jQuery('div.form-row.field-publish_date').attr('hidden', true);
        jQuery('div.form-row.field-unpublish_date').attr('hidden', true);

    }
    if (jQuery('#id_publication_1').attr('checked')) {
        jQuery('div.form-row.field-published').attr('hidden', true);
    }

    jQuery('input[name=publication]').change(function () {
        if (jQuery('#id_publication_0').attr('checked')) {
            jQuery('div.form-row.field-publish_date').attr('hidden', true);
            jQuery('div.form-row.field-unpublish_date').attr('hidden', true);
            jQuery('div.form-row.field-published').attr('hidden', false);
        };
        if (jQuery('#id_publication_1').attr('checked')) {
            jQuery('div.form-row.field-publish_date').attr('hidden', false);
            jQuery('div.form-row.field-unpublish_date').attr('hidden', false);
            jQuery('div.form-row.field-published').attr('hidden', true);
        };
    });
});

