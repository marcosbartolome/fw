#!/usr/bin/python
# -*- coding: utf-8 -*-

from optparse import make_option
from django.core.management.base import BaseCommand, CommandError
import urllib2
from xml.dom.minidom import parse, parseString
from django.conf import settings
from django.template import loader
import os
from familywealth.models import Invitation
import csv
#python manage.py  sync_ibex
# Class MUST be named 'Command'
class Command(BaseCommand):
    
    # Displayed from 'manage.py help mycommand'
    help = "Enviar invitaciones"

    option_list = BaseCommand.option_list + (make_option('--file', '-f', dest='emails_file',help='-f fichero_de_emails'),
                            )
    def handle(self, *app_labels, **options):
        if options['emails_file']:
            try:
                f = open(options['emails_file'], 'r' )
                reader = csv.reader( f, delimiter=';', dialect='excel' )
               # entries = []
               # emails_file = open(options['emails_file'],'r')
                reader.next()
                for row in reader:
                    email = row[3].strip()
                    print email
                
                    invitation = Invitation(email=email)
                    invitation.save()
            except IOError, e:
                print "Error, fichero no encontrado"
        else:
            print "Error, seleccione un fichero con el listado de emails (opción -f)"
