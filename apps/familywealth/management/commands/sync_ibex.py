#!/usr/bin/python
# -*- coding: utf-8 -*-

from optparse import make_option
from django.core.management.base import BaseCommand, CommandError
import urllib2
from xml.dom.minidom import parse, parseString
from django.conf import settings
from django.template import loader
import os

#python manage.py  sync_ibex
# Class MUST be named 'Command'
class Command(BaseCommand):

    # Displayed from 'manage.py help mycommand'
    help = "Sync ibex"

    def handle(self, *app_labels, **options):

        u1=urllib2.urlopen(settings.IBEX_URL)
        dom=parse(u1)

        min_di = max_di = min_di_val = max_di_val = None

        VALID_NAMES={
            'IBEX 35':'IBEX 35', 
            'DOW JONES INDUSTRIAL AVG.':'DOW JONES', 
            'NASDAQ 100 INDEX':'NASDAQ 100', 
            'STXE 50 EUR (Pr.)':'STOXX 50', 
            'DAX (PERFORMANCEINDEX)':'DAX', 
            'CAC 40 PARIS':'CAC 40', 
            'PSI-20':'PSI-20', 
            'EURONEXT 100':'EURONEXT 100', 
            'IPSA SANTIAGO DE CHILE':'IPSA CHILE',
        }
            
        di_nodes = sorted(dom.getElementsByTagName('DXC'), key=lambda x: x.childNodes[0].nodeValue.replace(".","").replace(",","."),reverse=True)
        di_nodes = [di_node.parentNode for di_node in di_nodes if di_node.parentNode.getElementsByTagName('NOM')[0].childNodes[0].nodeValue in VALID_NAMES.keys()]        

        di_nodes_clean = []
        for di_node in di_nodes:
            di_node_dict = {}
            di_node_dict['NOM'] = VALID_NAMES[di_node.getElementsByTagName('NOM')[0].childNodes[0].nodeValue]
            di_node_dict['HOR'] = di_node.getElementsByTagName('HOR')[0].childNodes[0].nodeValue
            for key in ['ULT', 'DXC']:
                di_node_dict[key] = float(di_node.getElementsByTagName(key)[0].childNodes[0].nodeValue.replace(".","").replace(",","."))
            di_nodes_clean.append(di_node_dict)

        context = dict(nodes = di_nodes_clean)
        generated_html = loader.render_to_string('portlet_ibex.html', context)

        file = open(os.path.join(settings.TEMPLATE_DIRS[0],'portlet_ibex_generated.html'),"w")
        file.write(unicode(generated_html).encode("utf-8"))
        file.close()    
