from optparse import make_option
from django.core.management.base import BaseCommand, CommandError
from familywealth.models import RSSSource, RSSEntry
import datetime
from datetime import timedelta, datetime
from dateutil import parser
import calendar
import feedparser
from dateutil.tz import tzutc

#Feedparser required -> sudo easy_install feedparser
#python manage.py  sync_rss_sources

# Class MUST be named 'Command'
class Command(BaseCommand):

    # Displayed from 'manage.py help mycommand'
    help = "Sync rss entries"

    # make_option requires options in optparse format
    option_list = BaseCommand.option_list  + (
                        make_option('--myoption', action='store',
                            dest='myoption',
                            default='default',
                            help='Option help message'),
                  )

    def handle(self, *app_labels, **options):
        """
        app_labels - app labels (eg. myapp in "manage.py reset myapp")
        options - configurable command line options
        """

        # Return a success message to display to the user on success
        # or raise a CommandError as a failure condition
        if options['myoption'] == 'default':
            for source in RSSSource.objects.all():
                channels = feedparser.parse(source.source)
                url = ''
                summary = ''
                title = ''

                for entry in channels.entries:
                    if entry.get('esdestacado') == u'NO':
                        # continue
                        remote_id = entry.title
                        url = entry.linkpdf
                        summary = entry.description
                        title = entry.title
                        date = entry.updated
                    elif entry.get('esdestacado') == u'SI':
                        remote_id = entry.title
                        url = entry.linkpdf
                        summary = entry.description
                        title = entry.title
                        date = entry.updated
                    else:
                        try:
                            remote_id = unicode(entry.id, channels.encoding)[0:255]
                            url = unicode(entry.link, channels.encoding)
                            summary = unicode(entry.description, channels.encoding)
                            title = unicode(entry.title, channels.encoding)
                            date = unicode(entry.date, channels.encoding)
                        except:
                            remote_id = entry.id[0:255]
                            url = entry.link
                            summary = entry.description
                            title = entry.title
                            date = entry.date

                    try:
                        date = parser.parse(date)
                        date = datetime(date.year, date.month, date.day, date.hour, date.minute, date.second, tzinfo=tzutc())
                        date = datetime.fromtimestamp(calendar.timegm(date.timetuple()))
                    except Exception, e:
                        date = datetime.now()

                    entry = RSSEntry.objects.filter(source__id = source.id, remote_id = remote_id)
                    entry = len(entry)>0 and entry.get() or None

                    if not entry:
                        rss_entry = RSSEntry(
                            title = title,
                            remote_id = remote_id,
                            source = source, 
                            text = summary,
                            link =  url,
                            date = date,
                        )
                        rss_entry.save()
