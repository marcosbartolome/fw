#!/usr/bin/python
# -*- coding: utf-8 -*-

from django.core.management.base import BaseCommand
from django.contrib.auth.models import User
from django.template import loader, Context
import settings
import datetime

class Command(BaseCommand):
    
    help = "Generar informe con el último login de cada usuario"

    def handle(self, *app_labels, **options):
        t = loader.get_template('logins_report.html')
        u=User.objects.all().order_by('last_login')
        ll = []
        for user in u:
            ll.append([user.username, user.email, user.date_joined.strftime("%d/%m/%Y"), user.last_login.strftime("%d/%m/%Y - %H:%m")])

        data1 = t.render(Context({
            'c': ll,
        }))
 
        path = datetime.date.today().strftime("%d-%m-%Y") + '.html'
        handle1=open(settings.REPORTS_PATH + '/' + path,'wb')
        handle1.write(data1.encode('latin-1', 'ignore'))
        print data1.encode('latin-1', 'ignore')
        handle1.close();
        res = '{"success": true, "path": "'+ path + '"}'
        print res
        return res
