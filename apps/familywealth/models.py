#!/usr/bin/python
# -*- coding: utf-8 -*-

from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _
from django.utils.translation import ugettext
from django.template.defaultfilters import slugify
from tagging.fields import TagField
from django.contrib.auth.models import User, UserManager
import string
import random
from tagging.utils import parse_tag_input
from django.template import loader, Context
from django.core.mail import EmailMessage
from django.conf import settings
from django.core.urlresolvers import reverse
from django.utils.safestring import mark_safe
from familywealth.utils import sys_error_log
from mysqlfulltextsearch.search_manager import SearchManager
from PIL import Image, ImageOps
import datetime
import locale
from south.modelsinspector import add_introspection_rules
add_introspection_rules([], ["^tagging\.fields\.TagField"])
REASON_CHOICES = (
    ('0', _('Proponer un Tema o Encuentro digital')),
    ('1', _('Comentario, duda o sugerencia relativos a la comunidad')),
    ('2', _('Ponerse en contacto con un especialista de Family Wealth de BBVA Patrimonios')),
    ('3', _('Ponerse en contacto con un especialista del IESE')),
)

#######################################################################
# Utilites
#######################################################################

def slugify_uniquely(value, model, slugfield="slug"):
    """Returns a slug on a name which is unique within a model's table
       self.slug = SlugifyUniquely(self.name, self.__class__)
    """
    suffix = 0
    potential = base = slugify(value)
    if len(potential) == 0:
        potential = 'null'
    while True:
        if suffix:
                potential = "-".join([base, str(suffix)])
        if not model.objects.filter(**{slugfield: potential}).count():
                return potential
        # we hit a conflicting slug, so bump the suffix & try again
        suffix += 1

def delete_tagged_items(base_item):
    #Delete the TaggedItems
    pass
 #   ct = Content.objects.get_for_model(base_item.__class__)
 #   for tagged_item in TaggedItem.objects.filter(object_id=base_item.id, content_type=ct):
 #       tagged_item.delete()

def resize_image(file_path, width, height):
    #Generate image that fits exactly    
    image = Image.open(file_path)    
    
    if image.size[0]!=width and image.size[1]!=height:
        # ImageOps compatible mode
        if image.mode not in ("L", "RGB"):
            image = image.convert("RGB")                            
        image.thumbnail((width, height), Image.ANTIALIAS)
        
        #Create white borders if necesary
        white_canvas = Image.new("RGB", (width, height), "white")
        x_paste = (white_canvas.size[0] - image.size[0])/2
        y_paste = (white_canvas.size[1] - image.size[1])/2
        white_canvas.paste(image, (x_paste, y_paste))                                
        white_canvas.save(file_path, 'JPEG', quality=75)    
            
#######################################################################
# Models
#######################################################################       
        
class FHUser(User):
    # Use UserManager to get the create_user method, etc.
    objects = UserManager()    
    position = models.CharField(max_length=255, blank=True, verbose_name=u'cargo o descripción')
    newsletter = models.BooleanField(verbose_name=u'recibir newsletter')
    #We can have a local avatar or just an url (url has precedence)
    image = models.ImageField(upload_to=u'images', null=True, blank=True, max_length=1000, help_text="100x100 px", verbose_name=u'imagen')        
    image_url = models.CharField(max_length=500, blank=True, verbose_name=u'url de imagen')   
    is_bbva_user = models.BooleanField(verbose_name=u'usuario BBVA')
    is_iese_user = models.BooleanField(verbose_name=u'usuario IESE')

    def short_email(self):
        return self.email
    short_email.short_description = 'Correo electrónico'
    short_email.admin_order_field = 'email'
        
    def short_position(self):
        return self.position
    short_position.short_description = 'Cargo'
    short_position.admin_order_field = 'position'    
        
    def short_name(self):
        return self.first_name
    short_name.short_description = 'Nombre'
    short_name.admin_order_field = 'first_name'    

    def short_newsletter(self):
        if self.newsletter:
            return '<img alt="True" src="/media/img/admin/icon-yes.gif">'
        else:
            return '<img alt="False" src="/media/img/admin/icon-no.gif">'                  
    short_newsletter.short_description = 'Newsletter'
    short_newsletter.admin_order_field = 'newsletter'    
    short_newsletter.allow_tags = True        
    
    def short_is_bbva_user(self):
        if self.is_bbva_user:
            return '<img alt="True" src="/media/img/admin/icon-yes.gif">'
        else:
            return '<img alt="False" src="/media/img/admin/icon-no.gif">'            
    short_is_bbva_user.short_description = 'es BBVA'      
    short_is_bbva_user.admin_order_field = 'is_bbva_user'      
    short_is_bbva_user.allow_tags = True        

    def short_is_iese_user(self):
        if self.is_iese_user:
            return '<img alt="True" src="/media/img/admin/icon-yes.gif">'
        else:
            return '<img alt="False" src="/media/img/admin/icon-no.gif">'               
    short_is_iese_user.short_description = 'es IESE'        
    short_is_iese_user.admin_order_field = 'is_iese_user'      
    short_is_iese_user.allow_tags = True        

    class Meta:
        verbose_name = _(u'usuario')
        verbose_name_plural = _(u'usuarios')       

    def save(self):
        super(FHUser, self).save()       
        if self.image:
            resize_image(self.image.path, 100,100)

class SearchableText(models.Model):
    searchable_text = models.TextField()
    object_id = models.CharField(max_length=255)
    object_model = models.CharField(max_length=255)    

    objects = SearchManager()

    class Meta:
        db_table = u'searchable_text'
        ordering = ['id']
        verbose_name = _(u'searchable text')
        verbose_name_plural = _(u'searchable texts')       
        unique_together = ('object_id', 'object_model')

class Content(models.Model):      
    """
        Basic abstract content type extended by article, discussion and event
    """
    title = models.CharField(max_length=255, verbose_name=u'título')    
    slug = models.SlugField(max_length=255, unique=True)    
    
    short_description = models.CharField(max_length=500, verbose_name=u'descripción corta')    
    long_description = models.TextField(verbose_name=u'descripción larga')
    creation_date = models.DateTimeField(auto_now_add=True, verbose_name=u'fecha de creación')
    
    creator = models.ForeignKey(FHUser, verbose_name=u'creador')
    
    image = models.ImageField(upload_to=u'images', null=True, blank=True, max_length=1000, help_text="250x160px para artículos y 160x160 para eventos y encuentros digitales", verbose_name=u'imagen')    
    miniature = models.ImageField(upload_to=u'images', null=True, blank=True, max_length=1000, help_text="100x100px", verbose_name=u'miniatura')
    
    highlighted = models.BooleanField(verbose_name=u'destacado')
    
    def __unicode__(self):
        return unicode(self.title)

    def creado(self):
        if self.creation_date:
            return self.creation_date
        else:
            return ''
    creado.allow_tags = True

    def save(self):
        if not self.slug:
            self.slug = slugify_uniquely(self.title, self.__class__)
        
        super(Content, self).save()       
        
        searchable_text = "%s %s %s" % (self.title, self.short_description, self.long_description)
        searchable_object = SearchableText.objects.filter(object_id = self.id, object_model = self.__class__.__name__)
        searchable_object = len(searchable_object)==1 and searchable_object.get() or None
        if searchable_object:
            searchable_object.searchable_text = searchable_text
        else:
            searchable_object = SearchableText(
                object_id = self.id, 
                object_model = self.__class__.__name__,
                searchable_text = searchable_text,
            )
        searchable_object.save()

        if self.miniature:
            resize_image(self.miniature.path, 100,100)

    class Meta:
        db_table = u'content'
        ordering = ['-creation_date']
        verbose_name = _(u'contenido')
        verbose_name_plural = _(u'contenidos')       

class Notification(models.Model):      
    content = models.ForeignKey(Content, related_name='notifications', verbose_name=u'contenido')
    user = models.ForeignKey(FHUser, related_name='notifications', verbose_name=u'usuario')

    def __unicode__(self):
        return unicode("%s -- %s" % (self.user.username, self.content.slug))

    class Meta:
        db_table = u'notification'
        ordering = ['user__username']
        verbose_name = _(u'suscripción a nuevo contenido')
        verbose_name_plural = _(u'suscripciones a nuevos contenidos')       
        unique_together = ('user', 'content')

        
class Comment(models.Model):      
    content = models.ForeignKey(Content, related_name='comments', verbose_name=u'contenido')
    user = models.ForeignKey(FHUser, related_name='comments', verbose_name=u'usuario')
    text = models.TextField(blank=True, verbose_name=u'texto')
    parent_comment = models.ForeignKey('self', null=True, blank=True, related_name='comments', verbose_name=u'en respuesta a') #OJO Comentario PADRE
    creation_date = models.DateTimeField(auto_now_add=True, verbose_name=u'fecha de creación')
    published = models.BooleanField(default=False)

    #Tags can't be part of Comment :(
    
    def parent_distance(self):
        if self.parent_comment==None:
            return [self.id]
        else:
            return self.parent_comment.parent_distance() + [self.id]

    def __unicode__(self):
        return unicode("%s -- %s -- %s -- %s" % (self.creation_date, self.user.username,self.content.slug, self.id))

    def save(self):

        super(Comment, self).save()       

        searchable_text = self.text
        searchable_object = SearchableText.objects.filter(object_id = self.id, object_model = self.__class__.__name__)
        searchable_object = len(searchable_object)==1 and searchable_object.get() or None
        if searchable_object:
            searchable_object.searchable_text = searchable_text
        else:
            searchable_object = SearchableText(
                object_id = self.id, 
                object_model = self.__class__.__name__,
                searchable_text = searchable_text,
            )
        searchable_object.save()

    class Meta:
        db_table = u'comment'
        ordering = ['-creation_date',]
        verbose_name = _(u'comentario')
        verbose_name_plural = _(u'comentarios')       
        
class Valoration(models.Model):      
    content = models.ForeignKey(Content, related_name='valorations', verbose_name=u'contenido')
    user = models.ForeignKey(FHUser, related_name='valorations', verbose_name=u'usuario')

    VALUE_CHOICES = (
        (1, 1),
        (2, 2),
        (3, 3),
        (4, 4),
        (5, 5),
    )
    value = models.IntegerField(choices=VALUE_CHOICES, verbose_name=u'valor')
    
    def __unicode__(self):
        return "%s-%s"%(unicode(self.user), unicode(self.content))

    class Meta:
        db_table = u'valoration'
        verbose_name = _(u'valoración')
        verbose_name_plural = _(u'valoraciones')       
        unique_together = ('user', 'content')

class Favourite(models.Model):      
    content = models.ForeignKey(Content, related_name='favourites', verbose_name=u'contenido')
    user = models.ForeignKey(FHUser, related_name='favourites', verbose_name=u'usuario')
    
    def __unicode__(self):
        return "%s-%s"%(unicode(self.user), unicode(self.content))

    class Meta:
        db_table = u'favourite'
        verbose_name = _(u'favorito')
        verbose_name_plural = _(u'favoritos')       
        unique_together = ('user', 'content')

class Category(models.Model):
    title = models.CharField(max_length=255, verbose_name=u'título')    
    slug = models.SlugField(max_length=255, unique=True)   
    description = models.CharField(max_length=500, blank=True, verbose_name=u'descripción')    

    def __unicode__(self):
        return unicode(self.title)

    def save(self):
        if not self.slug:
            self.slug = slugify_uniquely(self.title, self.__class__)
        super(Category, self).save()       
    
    class Meta:
        db_table = u'category'
        ordering = ['title']
        verbose_name = _(u'categoría')
        verbose_name_plural = _(u'categorías')            

class Article(Content):
    category = models.ForeignKey(Category, related_name='articles', verbose_name=u'categoría')
    
    TYPE_CHOICES = (
        ('0', _(u'Archivo')),
        ('1', _(u'Enlace')),
        ('2', _(u'Vídeo')),
    )
    C_CHOICES = (
        ('M', 'Manual'),
        ('P', 'Programada'),
    )
   
    type = models.CharField(max_length=1, choices=TYPE_CHOICES, verbose_name=u'tipo', blank=True)
    
    file = models.FileField(upload_to=u'files', null=True, blank=True, max_length=10000, verbose_name=u'archivo')
    link = models.CharField(max_length=255, blank=True, verbose_name=u'enlace')    
    video = models.CharField(max_length=255, blank=True, verbose_name=u'vídeo')    
        
    tags = TagField() #To get tags with articles:  Tag.objects.annotate(count=Count("items")).filter(count__gt=0)

    publication = models.CharField(max_length=1, choices=C_CHOICES, default='M', blank=False, null=False, verbose_name=u'publicación')
    published = models.BooleanField(default=True, verbose_name=u'Publicado')
    publish_date = models.DateTimeField(default=datetime.datetime.now, verbose_name=u'Publicado desde', blank=True, null=True)
    unpublish_date = models.DateTimeField(verbose_name=u'Publicado hasta', blank=True, null=True)

    class Meta:
        db_table = u'article'
        ordering = ['-creation_date']
        verbose_name = _(u'artículo')
        verbose_name_plural = _(u'artículos')            
        
    def delete(self, *args, **kwargs):        
        #Delete the TaggedItems
        delete_tagged_items(self)
        super(self.__class__, self).delete(*args, **kwargs) # Call the "real" delete() method.      
        

    def save(self):
        super(Article, self).save()       
        if self.image:
            resize_image(self.image.path, 250,160)

    def get_published_or_nothing(self):
        return self.published


class Discussion(Content):
    is_open=models.BooleanField(verbose_name=u'Abierto')
    tags = TagField() #To get tags with articles:  Tag.objects.annotate(count=Count("items")).filter(count__gt=0)
    file = models.FileField(upload_to=u'files', null=True, blank=True, max_length=10000, verbose_name=u'archivo')

    def delete(self, *args, **kwargs):        
        #Delete the TaggedItems
        delete_tagged_items(self)
        super(self.__class__, self).delete(*args, **kwargs) # Call the "real" delete() method.          
    
    class Meta:
        db_table = u'discussion'
        ordering = ['-creation_date']
        verbose_name = _(u'encuentro digital')
        verbose_name_plural = _(u'encuentros digitales')     

    def save(self):
        super(Discussion, self).save()       
        if self.image:
            resize_image(self.image.path, 160,160)

class Participant(models.Model):
    user = models.ForeignKey(FHUser, related_name='participants', verbose_name=u'usuario')
    discussion = models.ForeignKey(Discussion, related_name='participants', verbose_name=u'encuentro digital')

    def __unicode__(self):
        return "%s-%s" %(unicode(self.user), unicode(self.discussion))

    class Meta:
        db_table = u'participant'
        ordering = ['user']
        verbose_name = _(u'participante')
        verbose_name_plural = _(u'participantes')            
        unique_together = ('user', 'discussion')
        
class Event(Content):
    #La imagen de evento es 157x157
    date = models.DateTimeField(verbose_name=u'fecha de celebración')
    location = models.CharField(max_length=255, blank=True, verbose_name=u'localización') 
    tags = TagField() #To get tags with articles:  Tag.objects.annotate(count=Count("items")).filter(count__gt=0)
    planning = models.TextField(verbose_name=u'programa')
    file = models.FileField(upload_to=u'files', null=True, blank=True, max_length=10000, verbose_name=u'archivo')

    def delete(self, *args, **kwargs):        
        #Delete the TaggedItems
        delete_tagged_items(self)
        super(self.__class__, self).delete(*args, **kwargs) # Call the "real" delete() method.          
    
    class Meta:
        db_table = u'event'
        ordering = ['-creation_date']
        verbose_name = _(u'evento')
        verbose_name_plural = _(u'eventos')     
        
    def save(self):
        super(Event, self).save()       
        if self.image:
            resize_image(self.image.path, 160,160)        
        
class EventLink(models.Model):
    event = models.ForeignKey(Event, related_name='links', verbose_name=u'evento')    
    title = models.CharField(max_length=255, verbose_name=u'título')    
    description = models.CharField(max_length=500, blank=True, verbose_name=u'descripción')    
    link = models.CharField(max_length=255, verbose_name=u'enlace')    

    def __unicode__(self):
        return unicode(self.title)    
    
    class Meta:
        db_table = u'event_link'
        ordering = ['title']
        verbose_name = _(u'enlace para evento')
        verbose_name_plural = _(u'enlaces para eventos')     

class EventFile(models.Model):
    event = models.ForeignKey(Event, related_name='files', verbose_name=u'evento')    
    title = models.CharField(max_length=255, verbose_name=u'título')    
    description = models.CharField(max_length=500, blank=True, verbose_name=u'descripción')    
    file = models.FileField(upload_to=u'files', max_length=10000, verbose_name=u'archivo')

    def __unicode__(self):
        return unicode(self.title)    
            
    class Meta:
        db_table = u'event_file'
        ordering = ['title']
        verbose_name = _(u'archivo para evento')
        verbose_name_plural = _(u'archivos para eventos')     

class EventImage(models.Model):
    event = models.ForeignKey(Event, related_name='images', verbose_name=u'evento')    
    title = models.CharField(max_length=255, verbose_name=u'título')    
    description = models.CharField(max_length=500, blank=True, verbose_name=u'descripción')    
    image = models.ImageField(upload_to=u'images', max_length=10000, verbose_name=u'imagen')
    miniature = models.ImageField(upload_to=u'images', max_length=10000, verbose_name=u'miniatura', help_text="100x100 px")

    def __unicode__(self):
        return unicode(self.title)    
            
    class Meta:
        db_table = u'event_image'
        ordering = ['title']
        verbose_name = _(u'imagen para evento image')
        verbose_name_plural = _(u'imágenes para eventos')     

    def save(self):
        super(EventImage, self).save()       

        if self.miniature:
            resize_image(self.miniature.path, 100,100)
        
class Assistant(models.Model):
    user = models.ForeignKey(FHUser, related_name='assistants', verbose_name=u'usuario')
    event = models.ForeignKey(Event, related_name='assistants', verbose_name=u'evento')

    STATUS_CHOICES = (
        ('0', _('Yes')),
        ('1', _('No')),
    )
    status = models.CharField(max_length=1, choices=STATUS_CHOICES, verbose_name=u'asistencia', blank=True)

    def __unicode__(self):
        return "%s-%s" %(unicode(self.user), unicode(self.event))
    
    class Meta:
        db_table = u'assistant'
        ordering = ['user']
        verbose_name = _(u'asistente')
        verbose_name_plural = _(u'asistentes')            
        unique_together = ('user', 'event')
        
class Newsletter(models.Model):
    title = models.CharField(max_length=255, verbose_name=u'título')    
    slug = models.SlugField(max_length=255, unique=True)    
    description = models.CharField(max_length=500, blank=True, verbose_name=u'descripción')        
    events = models.ManyToManyField(Event, related_name='newsletters', null=True, blank=True, verbose_name=u'eventos')
    articles = models.ManyToManyField(Article, related_name='newsletters', null=True, blank=True, verbose_name=u'artículos')
    discussions = models.ManyToManyField(Discussion, related_name='newsletters', null=True, blank=True, verbose_name=u'encuentros digitales')
    date = models.DateTimeField(verbose_name=u'fecha de envío', editable=False,null=True, blank=True)
    sent = models.BooleanField(verbose_name=u'enviado')
    
    def __unicode__(self):
        return unicode(self.title)

    def preview(self):
        url = reverse('newsletter', args=[self.slug])
        url = '<a href="%s" target="_blank">%s</a>' % (url, url)
        return mark_safe(url)
    preview.allow_tags = True        
    preview.short_description = "Vista Previa"
    preview.admin_order_field = 'slug'
    
    def show_date(self):
        return self.date or ''
    show_date.short_description = "Fecha de envío"
    show_date.admin_order_field = 'date'
    
        
    def save(self):
        if not self.slug:
            self.slug = slugify_uniquely(self.title, self.__class__)
        super(Newsletter, self).save()      

    class Meta:
        db_table = u'newsletter'
        ordering = ['sent', '-date']
        verbose_name = _(u'newsletter')
        verbose_name_plural = _(u'newsletters')            

    def send(self,email, request):
        try:
            #Send email
            template_obj = loader.get_template("newsletter.html")

            base_host = request.build_absolute_uri('/')
            base_host = base_host[0:base_host.rfind("/")]

            context = Context(dict(
                newsletter = self,
                BASE_HOST = base_host,
            ))
            
            email_obj = EmailMessage(
              subject = _(u"Newsletter"),
              body    = template_obj.render(context),
              to      = [email],
              from_email = settings.DEFAULT_FROM_EMAIL,
            )
            
            email_obj.content_subtype = "html"
            email_obj.send()
        except Exception, e:            
            sys_error_log(e)

def random_code():
    potential = base = ''.join(random.choice(string.letters) for i in xrange(50))
    suffix=0
    while True:
        if suffix:
                potential = "-".join([base, str(suffix)])
        if not Invitation.objects.filter(**{'code': potential}).count():
                return potential
        # we hit a conflicting slug, so bump the suffix & try again
        suffix += 1    
    return 

class Invitation(models.Model):
    code = models.CharField(max_length=255, null=True, blank=True, unique=True, default=random_code, verbose_name=u'código')    
    email = models.EmailField(verbose_name=u'email')
    user = models.ForeignKey(FHUser, related_name='invitations', null=True, blank=True, verbose_name=u'usuario', editable=False)
    creation_date = models.DateTimeField(auto_now_add=True, verbose_name=u'fecha de creación')
    activation_date = models.DateTimeField(blank=True, null=True, verbose_name=u'fecha de activación', editable=False)

    def save(self):
        super(Invitation, self).save()       
        if not self.user:
            #Send email to user
            template_obj = loader.get_template("email/new_invitation.html")
            mail_context = Context(dict(code=self.code, BASE_HOST=settings.BASE_HOST))

            email_obj = EmailMessage(
              subject = _(u"Invitación a family wealth"),
              body    = template_obj.render(mail_context),
              to      = [self.email],
              from_email = settings.DEFAULT_FROM_EMAIL,
            )
            email_obj.content_subtype = "html"
            email_obj.send()             
        
    def fecha_de_activacion(self):
        if self.activation_date:
            return self.activation_date
        else:
            return ''
    fecha_de_activacion.short_description = 'Fecha de activación'
    fecha_de_activacion.admin_order_field = 'activation_date'    
    
    def friendly_user(self):
        return self.user or ''
    friendly_user.short_description = 'Usuario'    
    friendly_user.admin_order_field = 'user'

    def __unicode__(self):
        return unicode(self.code)    

    def usuario(self):
        if self.user:
            return '<a href="/admin/familywealth/fhuser/%s">%s</a>'%(self.user.id, self.user.username)
        else:
            return ''
    usuario.allow_tags = True        

    class Meta:
        db_table = u'invitation'
        ordering = ['-activation_date']
        verbose_name = _(u'invitación')
        verbose_name_plural = _(u'invitaciones')            
        
class FeedbackNotificacion(models.Model):
    user = models.ForeignKey(FHUser, related_name='feedback_notifications', null=True, blank=True, verbose_name=u'usuario')
    reason = models.CharField(max_length=1, choices=REASON_CHOICES, verbose_name=u'razón')
    text = models.TextField(blank=True, verbose_name=u'texto')
    answered = models.BooleanField(verbose_name=u'respondido')
    
    def __unicode__(self):
        max_short_length=50
        short_desc = self.text[:max_short_length]
        if len(self.text)>max_short_length:
            short_desc+="..."            
        return unicode(unicode(self.user or self.id) + " - " + short_desc)    
    
    class Meta:
        db_table = u'feedback_notification'
        ordering = ['user']
        verbose_name = _(u'petición de usuario')
        verbose_name_plural = _(u'peticiones de usuarios')                

class RSSSource(models.Model):
    title = models.CharField(max_length=255, verbose_name=u'título')    
    slug = models.SlugField(max_length=255, unique=True)    
    source = models.CharField(max_length=255, verbose_name=u'url de origen')    
    short_description = models.CharField(max_length=500, blank=True, verbose_name=u'descripción corta')    
    long_description = models.TextField(blank=True, verbose_name=u'descripción larga')
    image = models.ImageField(upload_to=u'images', null=True, blank=True, max_length=1000, help_text="100x100px", verbose_name=u'imagen')        

    def __unicode__(self):
        return unicode(self.title)    
    class Meta:
        db_table = u'rss_source'
        ordering = ['title']
        verbose_name = _(u'fuente de rss')
        verbose_name_plural = _(u'fuentes de rss')            

    def save(self):
        if not self.slug:
            self.slug = slugify_uniquely(self.title, self.__class__)
        super(RSSSource, self).save()      
        if self.image:
            resize_image(self.image.path, 100, 100)        

class RSSEntry(models.Model):
    title = models.CharField(max_length=255, verbose_name=u'título')    
    slug = models.SlugField(max_length=255, unique=True)    
    remote_id = models.CharField(max_length=255, verbose_name=u'identificador de rss')    
    source = models.ForeignKey(RSSSource, verbose_name=u'fuente de rss')    
    text = models.TextField(blank=True, verbose_name=u'texto')
    link = models.CharField(max_length=255, verbose_name=u'enlace') 
    date = models.DateTimeField(verbose_name=u'fecha')
    
    def __unicode__(self):
        return unicode(self.title)    

    class Meta:
        db_table = u'rss_entry'
        ordering = ['date']
        verbose_name = _(u'entrada de rss')
        verbose_name_plural = _(u'entradas de rss')        
        unique_together = ('source', 'remote_id')

    def save(self):
        if not self.slug:
            self.slug = slugify_uniquely(self.title, self.__class__)
        super(RSSEntry, self).save()      

# creamos los campos en el admin "campo titulo" y el campo "con titulo y comentario" 

class StaticContent(models.Model):
    title = models.CharField(max_length=255)    
    slug = models.SlugField(max_length=255, unique=True) 
    order = models.PositiveIntegerField(default=0)
# esto sirve para crear los menus del pie    
    def get_absolute_url(self):
        return '/info/' + self.slug
    
class StaticContentBlock(models.Model):
    title =  models.CharField(max_length=255, null=True, blank=True)
    content = models.TextField()
    static_content = models.ForeignKey(StaticContent)
    order = models.PositiveIntegerField(default=0)
    
    
# invitacion privada

#class Invitation(models.Model):
 #   email = models.EmailField(max_length=100)
 #   hash = models.CharField(max_length=15)
    
