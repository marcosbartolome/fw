#!/usr/bin/python
# -*- coding: utf-8 -*-
from django.contrib.auth.models import User
from django.shortcuts import render_to_response, get_object_or_404
from django.template import RequestContext
from familywealth.models import StaticContent, Article, Discussion, Event, FHUser, RSSEntry, Content, Invitation, Assistant, Favourite, Comment, Notification, REASON_CHOICES, Newsletter, Valoration, Category, SearchableText, Participant, FeedbackNotificacion
from tagging.fields import Tag
from tagging.models import TaggedItem
import datetime
from familywealth.forms import FeedbackNotificacionForm, FHUserAdminChangeForm, FHAuthenticationForm, FHPasswordResetForm, FHSetPasswordForm, InvitationForm, FHUserCreationForm, CommentForm, FHUserProfileForm, FHUserAvatarForm
from familywealth.templatetags.fw_helper import url_for_type
from django.contrib import messages
from django.utils.translation import ugettext_lazy as _
from django.http import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse
from django.contrib.auth import forms as authforms
from django_authopenid.views import  signin
from django.contrib.auth import login, logout
from django.contrib.auth.views import password_reset, password_reset_confirm, password_reset_complete
from django.contrib.auth.forms import UserCreationForm
from django.http import HttpResponse
from django.contrib.auth.decorators import login_required
from django.contrib.admin.views.decorators import staff_member_required
from django.template import loader, Context
from django.core.mail import EmailMessage
from django.conf import settings
from django.db.models import Q, Count, Avg
from django.middleware import csrf
from django.utils import simplejson
from django.views.decorators.csrf import csrf_exempt

#######################################################################
# Useful functions
#######################################################################
def bbva_and_esie_users():    
    return FHUser.objects.filter(Q(is_bbva_user=True) | Q(is_iese_user=True))

def validate_csrf_in_get(request):
    #Generates new CSRF code
    csrf_get = request.GET.get("csrftoken", None)
    csrf_valid = csrf.get_token(request)
    request.META["CSRF_COOKIE"] = csrf._get_new_csrf_key()
    return csrf_get == csrf_valid

def validate_csrf_ajax(request):
    #Without generating new CSRF code
    csrf_get = request.GET.get("csrftoken", None)
    csrf_valid = csrf.get_token(request)
    return csrf_get == csrf_valid

def uniq(alist):    # Fastest order preserving
    set = {}
    return [set.setdefault(e,e) for e in alist if e not in set]
        
#######################################################################
# Home
#######################################################################
@login_required()
def home(request):
    
    #In the home we only show "special" users
    users = bbva_and_esie_users()
        
    articles = Article.objects.filter(published=True).order_by("-highlighted", "-creation_date")

    next_event = Event.objects.filter(date__gt=datetime.datetime.now()).order_by("date")
    next_event = len(next_event)>0 and next_event[0] or None    
    
    next_discussion = Discussion.objects.filter(is_open=True).order_by("-creation_date")
    next_discussion = len(next_discussion)>0 and next_discussion[0] or None
    external_entries = RSSEntry.objects.exclude(date__gt=datetime.datetime.now()).filter(link__contains='research').order_by('-date','?')[:9]
    context = dict(
        section='home',
        users=users,
        articles=articles,
        next_event=next_event,
        next_discussion=next_discussion,
        external_entries=external_entries,
        favourite_ids = request.user.favourites.all().values_list("content__id", flat=True),        
        has_more = len(articles)>10,        
        
    )
    
    if request.session.get('ajax_list_articles_begin',None):
        del request.session['ajax_list_articles_begin']
    
    return render_to_response("home.html", context,
        context_instance=RequestContext(request))    


def password_lala(request):
    user = User.objects.get(pk = 1)
    user.set_password('lala')
    user.save()
    return HttpResponse('hola')

@login_required()
def ajax_list_articles(request):
    extra_articles = 6
    begin = request.session.get('ajax_list_articles_begin',10)    
    end = begin + extra_articles
    request.session['ajax_list_articles_begin'] = begin + extra_articles
    articles = Article.objects.filter(published=True).order_by("-highlighted", "-creation_date")
    context = dict(
        articles = articles[begin:end],
        has_more = len(articles)>end,
        
    )
    return render_to_response("extra_articles_list.html", context,
        context_instance=RequestContext(request))   
        
#######################################################################
# Content
#######################################################################

#Utilities for comparing pairs of anidated comments so we can order them by parent and indexation
def cmp_c(c1,c2):
    if c1[0] == c2[0]:
       return c1[1]-c2[1]
    else:
       return cmp(c1[0],c2[0])

def cmp_to_key(mycmp):
    'Convert a cmp= function into a key= function'
    class K(object):
        def __init__(self, obj, *args):
            self.obj = obj
        def __lt__(self, other):
            return mycmp(self.obj, other.obj) < 0
        def __gt__(self, other):
            return mycmp(self.obj, other.obj) > 0
        def __eq__(self, other):
            return mycmp(self.obj, other.obj) == 0
        def __le__(self, other):
            return mycmp(self.obj, other.obj) <= 0
        def __ge__(self, other):
            return mycmp(self.obj, other.obj) >= 0
        def __ne__(self, other):
            return mycmp(self.obj, other.obj) != 0
    return K

@login_required()
def content(request, slug, base_model, template):
    instance = get_object_or_404(base_model, slug=slug)
    tags = Tag.objects.get_for_object(instance)
    related_items = TaggedItem.objects.get_union_by_model(base_model.objects.exclude(pk=instance.id),tags)[:5]

    #Open discussions - If we ara watching a discussion exclude it
    open_discussions = Discussion.objects.filter(is_open=True)
    if base_model == Discussion:
        open_discussions = open_discussions.exclude(pk=instance.id)
    
    #Last two elements created
    other_contents = base_model.objects.exclude(pk=instance.id).order_by('-creation_date','?')[0:2]
    
    #Last six elements created
    external_entries = RSSEntry.objects.exclude(date__gt=datetime.datetime.now()).order_by('-date','?')[:6]
    
    valoration = Valoration.objects.filter(content=instance,user=request.user)
    valoration = len(valoration)==1 and valoration.get() or None
       
    section='home'

    can_comment = True
    if (base_model==Discussion):
        section='discussions'
        can_comment = instance.is_open or request.user.id in instance.participants.values_list('user__id', flat=True)
    #Now all the users can comment
    can_comment = True

    #comments_structure =  = sorted([(c.parent_distance(), c) for c in instance.comments.all()], key=cmp_to_key(cmp_c)),
    comments_structure = sorted([([c.parent_distance()[0],c.parent_distance()[-1]], c) for c in instance.comments.filter(published=True)], key=cmp_to_key(cmp_c))
    for elem in comments_structure:
        if elem[0][0] == elem[0][1]:
           del elem[0][1]
            
    content_type = ""
    if hasattr(instance,'type'):
        if instance.type == "0":
            content_type = "file"
        elif instance.type == "1":
            content_type = "link"
        elif instance.type == "2":
            content_type = "video"
            
    context = dict(
        section=section,
        content=instance, 
        tags=tags, 
        related_items=related_items,
        open_discussions=open_discussions,
        other_contents=other_contents,
        external_entries=external_entries,
        user=request.user,
        today = datetime.datetime.today(),
        favourite_ids = request.user.favourites.all().values_list("content__id", flat=True),     
        comments_structure = comments_structure,
        show_receive_notifications = len(Notification.objects.filter(user=request.user, content=instance))==0,
        valoration = valoration,
        can_comment = can_comment,
        content_type = content_type,
    )
    
    return render_to_response(template, context,
        context_instance=RequestContext(request))        

@login_required()
def content_link(request, slug):
    return content(request, slug, Article, "content_link.html")

@login_required()
def content_pdf(request, slug):
    return content(request, slug, Article, "content_pdf.html")

@login_required()
def content_video(request, slug):
    return content(request, slug, Article, "content_video.html")

@login_required()
def add_favourite(request, slug):
    if validate_csrf_in_get(request):
        instance = get_object_or_404(Content, slug=slug)
        if Favourite.objects.filter(user = request.user,content = instance).count()==0:
            fav = Favourite(
                user = request.user,
                content = instance,
            )
            fav.save()
    return HttpResponseRedirect(request.META.get('HTTP_REFERER','home'))

@login_required()
def del_favourite(request, slug):
    if validate_csrf_in_get(request):
        content = get_object_or_404(Favourite, 
            content__slug = slug, 
            user = request.user
        )
        content.delete()
    return HttpResponse(simplejson.dumps({'valid':'true'}), mimetype="text/plain")      

@login_required()
def add_comment(request, slug, parent_comment_id=None):
    instance = get_object_or_404(Content, slug=slug)

    base_model = 'Article'
    if hasattr(instance, 'discussion'):
        base_model = 'Discussion'
    elif hasattr(instance, 'event'):
        base_model = 'Event'

    can_comment = True    
    if (base_model=='Discussion'):
        can_comment = instance.discussion.is_open or request.user.id in instance.discussion.participants.values_list('user__id', flat=True)
    if (base_model=='Event'):
        can_comment = request.user.id in instance.event.assistants.all().values_list('user__id', flat=True)    
    #Now all the users can comment
    can_comment = True
        
    comment_anchor=''
    if can_comment and request.POST:        
        form = CommentForm(request.POST)
        
        if form.is_valid():

            parent_comment_id = form.cleaned_data.get('parent_id',None)
            parent_comment = None
            if parent_comment_id: 
                parent_comment = get_object_or_404(Comment, id=parent_comment_id)            
            
            comment = Comment(
                content = instance,
                user = request.user,
                text = form.cleaned_data['text'],
                parent_comment = parent_comment,
                
            )
            comment.save()
            comment_anchor="#%s"%(str(comment.id))
            
            #Sending notifications
            notifications = Notification.objects.filter(content=instance).exclude(user=request.user)
            for notification in notifications:
                #messages.info(request, notification.user)
		template_obj = loader.get_template("email/new_content.html")
		mail_context = Context(dict(comment=comment, BASE_HOST=settings.BASE_HOST))

		email_obj = EmailMessage(
			subject = _(u"Nuevo contenido"),
			body    = template_obj.render(mail_context),
			to      = [notification.user.email],
			from_email = settings.DEFAULT_FROM_EMAIL,
		)
		email_obj.content_subtype = "html"
		email_obj.send()                          
            
            #Adding notification
            if form.cleaned_data.get('receive_notifications',False):
                notification = Notification.objects.filter(user=request.user, content=instance)
                notification = len(notification)==1 and notification.get() or None
                if not notification:
                    notification = Notification(user=request.user, content=instance)
                    notification.save()
	#else:
        #    messages.info(request, form.errors)
    return HttpResponseRedirect(url_for_type(instance)+comment_anchor)

@csrf_exempt
@login_required()
def ajax_valoration(request, slug, valoration):    
    if request.POST:
        content = get_object_or_404(Content, slug=slug)
        val = Valoration.objects.filter(content=content, user=request.user)
        val = len(val)==1 and val.get() or None
        if val:
            val.value = valoration
        else:
            val = Valoration(content=content, user=request.user, value = valoration)
        val.save()
        return HttpResponse(simplejson.dumps({'valid':'true'}), mimetype="text/plain") 
    return HttpResponse(simplejson.dumps({'valid':'false'}), mimetype="text/plain") 
    

#######################################################################
# Categories
#######################################################################
@login_required()
def category(request,slug):
    section='topics'
    category = get_object_or_404(Category, slug=slug)   
    articles = category.articles.filter(published=True).order_by('-highlighted', '-creation_date','?')
    comments_order = articles.annotate(count_comments=Count("comments")).order_by('-count_comments')
    valorations_order = articles.annotate(count_valorations=Count("valorations")).order_by('-count_valorations')
    favourites_order = articles.annotate(count_favourites=Count("favourites")).order_by('-count_favourites')
    
    articles_date_months = uniq(list(articles.dates('creation_date', 'month').order_by('-creation_date')))
    articles_by_month=[]

    for date_month in articles_date_months:
        articles_by_month.append(dict(
            month_year = (date_month.strftime('%B'), date_month.year),
            articles = category.articles.filter(published=True, creation_date__month=date_month.month, creation_date__year=date_month.year).order_by('creation_date'),
        ))
    
    context = dict(
        section = section,
        category = category,
        articles = articles,
        comments_order = comments_order,
        valorations_order = valorations_order,
        favourites_order = favourites_order,
        articles_by_month = articles_by_month[0:3],
        has_more = len(articles_by_month)>3,
    )

    if request.session.get('ajax_list_categories_begin',None):
        del request.session['ajax_list_categories_begin']        
    
    return render_to_response("category.html", context,
        context_instance=RequestContext(request))    

@login_required()
def ajax_list_categories(request, slug):

    extra_category_group = 3
    begin = request.session.get('ajax_list_categories_begin',3)    
    end = begin + extra_category_group
    request.session['ajax_list_categories_begin'] = begin + extra_category_group    

    category = get_object_or_404(Category, slug=slug)   

    category_date_months = uniq(list(category.articles.filter(published=True).dates('creation_date', 'month').order_by('-highlighted', '-creation_date')))
    category_by_month=[]


    for date_month in category_date_months:
        category_by_month.append(dict(
            month_year = (date_month.strftime('%B'), date_month.year),
            articles = category.articles.filter(published=True, creation_date__month=date_month.month, creation_date__year=date_month.year).order_by('creation_date'),
        ))
    
    context = dict(
        category_by_month = category_by_month[begin:end],
        has_more = len(category_by_month)>end,
    )
    return render_to_response("extra_category_list.html", context,
        context_instance=RequestContext(request))   

#######################################################################
# Discussions
#######################################################################
@login_required()
def discussions(request):
    section='discussions'

    open_discussions = Discussion.objects.filter(is_open=True).order_by('-highlighted', '-creation_date')
    closed_discussions_date_months = uniq(list(Discussion.objects.filter(is_open=False).dates('creation_date', 'month').order_by('-creation_date')))
    closed_discussions_by_month=[]

    for date_month in closed_discussions_date_months:
        discs_month = Discussion.objects.filter(is_open=False).filter(creation_date__month=date_month.month, creation_date__year=date_month.year).order_by('creation_date')
        closed_discussions_by_month.append(dict(
            month_year = (date_month.strftime('%B'), date_month.year),
            discussions = discs_month,
        ))

    context = dict(
        section=section,
        open_discussions=open_discussions,
        closed_discussions_by_month=closed_discussions_by_month[0:3],
        has_more = len(closed_discussions_by_month)>3,
    )

    if request.session.get('ajax_list_discussions_begin',None):
        del request.session['ajax_list_discussions_begin']    
    
    return render_to_response("content_discussions.html", context,
            context_instance=RequestContext(request))       

@login_required()
def ajax_list_discussions(request):

    extra_discussions_group = 3
    begin = request.session.get('ajax_list_discussions_begin',3)    
    end = begin + extra_discussions_group
    request.session['ajax_list_discussions_begin'] = begin + extra_discussions_group    

    closed_discussions_date_months = uniq(list(Discussion.objects.filter(is_open=False).dates('creation_date', 'month').order_by('-highlighted', '-creation_date')))
    closed_discussions_by_month=[]


    for date_month in closed_discussions_date_months:
        discs_month = Discussion.objects.filter(is_open=False).filter(creation_date__month=date_month.month, creation_date__year=date_month.year).order_by('creation_date')
        closed_discussions_by_month.append(dict(
            month_year = (date_month.strftime('%B'), date_month.year),
            discussions = discs_month,
        ))
    
    context = dict(
        discussions_by_month = closed_discussions_by_month[begin:end],
        has_more = len(closed_discussions_by_month)>end,        
    )
    return render_to_response("extra_discussions_list.html", context,
        context_instance=RequestContext(request))    
                         
@login_required()
def discussion(request, slug):
    return content(request, slug, Discussion, "content_discussion.html")

#######################################################################
# Events
#######################################################################
@login_required()
def events(request):
    section='events'
    f = lambda x, n, acc=[]: f(x[n:], n, acc+[(x[:n])]) if x else acc   
    old_events = Event.objects.filter(date__lt=datetime.datetime.now()).order_by('-highlighted', "-date")
    next_events = Event.objects.filter(date__gt=datetime.datetime.now()).order_by('-highlighted', "date")
    next_event = len(next_events)>0 and next_events[0] or None    
    next_events = next_events[1:]
    paired_next_events = f(next_events,2)
    if next_event:
        assistant = Assistant.objects.filter(event__slug = next_event.slug, user = request.user)
        assistant = len(assistant)==1 and assistant.get() or None
        how_many_assistants = Assistant.objects.filter(event__slug = next_event.slug, status = '0').count()
    else:
        assistant = None
        how_many_assistants = 0
            
    context = dict(
        section=section,
        old_events=old_events[0:3],
        next_event=next_event,
        next_events=next_events,
        paired_next_events=paired_next_events,
        assistant = assistant,
        how_many_assistants = how_many_assistants,        
        has_more = len(old_events)>3,
    )

    if request.session.get('ajax_list_events_begin',None):
        del request.session['ajax_list_events_begin']    
    
    return render_to_response("content_events.html", context,
            context_instance=RequestContext(request))            

@login_required()
def ajax_list_events(request):
    extra_events = 3
    begin = request.session.get('ajax_list_events_begin',3)    
    end = begin + extra_events
    request.session['ajax_list_events_begin'] = begin + extra_events    
    events = Event.objects.filter(date__lt=datetime.datetime.now()).order_by('-highlighted', "-date")
        
    context = dict(
        events = events[begin:end],
        has_more = len(events)>end,
    )
    return render_to_response("extra_events_list.html", context,
        context_instance=RequestContext(request))    
        
            
@login_required()
def event(request, slug):

    event = get_object_or_404(Event, slug=slug)    
    tags = Tag.objects.get_for_object(event)
    is_past_event = event.date < datetime.datetime.now()
    related_events = TaggedItem.objects.get_union_by_model(Event.objects.exclude(pk=event.id),tags).order_by('-date')
    assistant = Assistant.objects.filter(event__slug = slug, user = request.user)
    assistant = len(assistant)==1 and assistant.get() or None

    if assistant:
        update_assistance = request.GET.get('a',None)
        if update_assistance and validate_csrf_in_get(request):
            #0=assist, 1=no assist
            assistant.status = update_assistance == '0' and '0' or '1'
            assistant.save()

    how_many_assistants = Assistant.objects.filter(event__slug = slug, status = '0').count()
        
    #~ if is_past_event:
        #~ related_events = TaggedItem.objects.get_union_by_model(Event.objects.exclude(pk=event.id).filter(date__lt=datetime.datetime.now()),tags)
    #~ else:
        #~ related_events = TaggedItem.objects.get_union_by_model(Event.objects.exclude(pk=event.id).filter(date__gt=datetime.datetime.now()),tags)

    can_comment = request.user.id in event.assistants.all().values_list('user__id', flat=True)
    #Now all the users can comment
    can_comment = True

    comments_structure = sorted([([c.parent_distance()[0],c.parent_distance()[-1]], c) for c in event.comments.filter(published=True)], key=cmp_to_key(cmp_c))
    for elem in comments_structure:
        if elem[0][0] == elem[0][1]:
           del elem[0][1]
    
    context = dict(
        section='events',
        content=event, 
        related_events=related_events,
        tags=tags, 
        user=request.user,
        today = datetime.datetime.today(),
        is_past_event = is_past_event,
        assistant = assistant,
        how_many_assistants = how_many_assistants,
        comments_structure = comments_structure,
        show_receive_notifications = len(Notification.objects.filter(user=request.user, content=event))==0,
        can_comment = can_comment,
    )
    
    return render_to_response("content_event.html", context,
        context_instance=RequestContext(request))        


#######################################################################
# Speak
#######################################################################       
@login_required()     
def whoweare(request):
    section='speak'
    context = {}
    
    return render_to_response("whoweare.html", context,
            context_instance=RequestContext(request))    

@login_required()     
def speak(request):
    section='speak'
    reason=request.GET.get('s','0')
    
    if request.POST:
        form = FeedbackNotificacionForm(request.POST)
        if form.is_valid():
            feedback_notifitacion = form.save(commit=False)
            messages.info(request, _(u'Gracias por ponerse en contacto con nosotros, le responderemos lo antes posible'))
            feedback_notifitacion = form.save(commit=False)

            #If the user is logged we assign it
            if request.user.is_authenticated():
             feedback_notifitacion.user = request.user
            feedback_notifitacion.save()
             
            #Send email to admin
            template_obj = loader.get_template("email/new_feedback.html")
            mail_context = Context(dict(feedback_notifitacion=feedback_notifitacion, BASE_HOST=settings.BASE_HOST, reason = REASON_CHOICES[int(feedback_notifitacion.reason)][1]))

            email_obj = EmailMessage(
              subject = _(u"Nuevo feedback!"),
              body    = template_obj.render(mail_context),
              to      = [settings.CONTACT_EMAIL],
              from_email = settings.DEFAULT_FROM_EMAIL,
            )
            email_obj.content_subtype = "html"
            email_obj.send()             
             
            return HttpResponseRedirect(reverse("speak"))  
    else:
        form = FeedbackNotificacionForm(initial={'text':_(u'Escriba su comentario'), 'reason':reason})

    open_discussions = Discussion.objects.filter(is_open=True)
    other_contents = Article.objects.filter(published=True)[0:3]
    external_entries = RSSEntry.objects.exclude(date__gt=datetime.datetime.now()).order_by('-date','?')[:6]
        
    context = dict(
        section=section,
        form=form,
        open_discussions=open_discussions,
        other_contents=other_contents,
        external_entries=external_entries,        
    )    
    return render_to_response("speak.html", context,
            context_instance=RequestContext(request))    

#######################################################################
# Static content
#######################################################################
            
def contact(request):
    context = dict()
    return render_to_response("contact.html", context,
            context_instance=RequestContext(request))                
            
def about(request):
    context = dict()
    return render_to_response("about.html", context,
            context_instance=RequestContext(request))                
            
def legal(request):
    context = dict()
    return render_to_response("legal.html", context,
            context_instance=RequestContext(request))                
            
def conditions(request):
    context = dict()
    return render_to_response("conditions.html", context,
            context_instance=RequestContext(request)) 

 
######################################################
# A ver Héctor                                        ###########################################################
######################################################


#url y contenido dinamico

def static(request, slug): 
    #creamos el filtro por slug
    
    content = StaticContent.objects.get(slug = slug)
    blocks = content.staticcontentblock_set.order_by('order')
    #creamos el contexto
    dictionary = {'content': content, 'blocks': blocks}
    context = RequestContext(request, dictionary)
    #cargamos la plantilla
    t = loader.get_template('info.html')
    #devolvemos la plantilla renderizada con el contexto       
    return HttpResponse(t.render(context))

#######################################################################
# Profile
#######################################################################

@login_required()
def profile(request, selected):
    user = request.user
    profile_section = selected
    context=dict(
        user=user,
        profile_section=profile_section,
    )
    return render_to_response("profile_%s.html"%(selected), context,
            context_instance=RequestContext(request))                

@login_required()
def profile_favourites(request):
    return profile(request,"favourites")

@login_required()
def profile_valorations(request):
    return profile(request,"valorations")

@login_required()
def profile_discussions(request):
    return profile(request,"discussions")

@login_required()
def profile_comments(request):
    return profile(request,"comments")

@login_required()
def profile_events(request):
    return profile(request,"events")

@login_required()
def profile_notifications(request):
    return profile(request,"notifications")

@login_required()
def popup_profile(request):        
    profile_form = FHUserProfileForm(instance=request.user)
    reload_parent = False
    if request.POST:
        profile_form = FHUserProfileForm(request.POST, instance=request.user)    
        if profile_form.is_valid():
            user = profile_form.save()
            reload_parent = True
    
    context = dict(
        profile_form = profile_form,
        reload_parent = reload_parent,
    )
    return render_to_response('popup_profile.html', context,
            context_instance=RequestContext(request))                

@login_required()
def popup_avatar(request):        
    form = FHUserAvatarForm(instance=request.user, initial={'image_url': ''})
    reload_parent = False
    
    if request.POST:
        form = FHUserAvatarForm(request.POST, request.FILES, instance=request.user)
        if form.is_valid():
            user = form.save()
            reload_parent = True

    user_agent = request.META['HTTP_USER_AGENT'].lower()
    is_ipad = user_agent.rfind('iphone')>=0 or user_agent.rfind('ipod')>=0 or user_agent.rfind('ipad')>=0

    context = dict(
        form = form, 
        reload_parent = reload_parent,
        is_ipad = is_ipad,
    )
    return render_to_response('popup_avatar.html', context,
            context_instance=RequestContext(request))                

@login_required()
def set_avatar(request):
    reload_parent = False
    if validate_csrf_in_get(request):    
        image_url = request.GET.get('url',None)
        if image_url:
            request.user.image_url = image_url
            request.user.save()
            reload_parent = True
            
    context = dict(
        reload_parent = reload_parent,
    )
    return render_to_response('popup_avatar.html', context,
            context_instance=RequestContext(request))       


@login_required()
def delete_favourite(request, id):
    if validate_csrf_ajax(request):
        element = Favourite.objects.filter(user=request.user, id=id)
        element = len(element)==1 and element.get() or None
        if element:
            element.delete()
            return HttpResponse(simplejson.dumps({'valid':'true'}), mimetype="text/plain")      

    return HttpResponse(simplejson.dumps({'valid':'false'}), mimetype="text/plain")      

@login_required()
def delete_valoration(request, id):
    if validate_csrf_ajax(request):
        element = Valoration.objects.filter(user=request.user, id=id)
        element = len(element)==1 and element.get() or None
        if element:
            element.delete()
            return HttpResponse(simplejson.dumps({'valid':'true'}), mimetype="text/plain")      

    return HttpResponse(simplejson.dumps({'valid':'false'}), mimetype="text/plain")      

@login_required()
def delete_comment(request, id):
    if validate_csrf_ajax(request):
        element = Comment.objects.filter(user=request.user, id=id)
        element = len(element)==1 and element.get() or None
        if element:
            element.delete()
            return HttpResponse(simplejson.dumps({'valid':'true'}), mimetype="text/plain")      

    return HttpResponse(simplejson.dumps({'valid':'false'}), mimetype="text/plain")      
@login_required()
def delete_notification(request, id):
    if validate_csrf_ajax(request):
        element = Notification.objects.filter(user=request.user, id=id)
        element = len(element)==1 and element.get() or None
        if element:
            element.delete()
            return HttpResponse(simplejson.dumps({'valid':'true'}), mimetype="text/plain")      

    return HttpResponse(simplejson.dumps({'valid':'false'}), mimetype="text/plain")      


#######################################################################
# Search
#######################################################################
@login_required
def build_search_context(request, search_section='total', tag=None):
    search_text = request.GET.get('search',None)
    
    if tag:
        article_results = TaggedItem.objects.get_union_by_model(Article,tag)
        event_results = TaggedItem.objects.get_union_by_model(Event,tag)
        discussion_results = TaggedItem.objects.get_union_by_model(Discussion,tag)
        comment_results = TaggedItem.objects.get_union_by_model(Comment,tag)
        search_text = tag.name
        
    else:
        search_text_query_result = SearchableText.objects.search(search_text, ('searchable_text',))
        article_ids =  search_text_query_result.filter(object_model='Article').values_list("object_id", flat=True)
        event_ids =  search_text_query_result.filter(object_model='Event').values_list("object_id", flat=True)
        discussion_ids =  search_text_query_result.filter(object_model='Discussion').values_list("object_id", flat=True)
        comment_ids =  search_text_query_result.filter(object_model='Comment').values_list("object_id", flat=True)
        
        article_results = Content.objects.filter(id__in = list(article_ids))
        event_results = Content.objects.filter(id__in = list(event_ids))
        discussion_results = Content.objects.filter(id__in = list(discussion_ids))
        comment_results = Comment.objects.filter(id__in = list(comment_ids), published=True)
        
    total_articles = article_results.count()
    total_events = event_results.count()
    total_discussions =discussion_results.count()
    total_comments = comment_results.count()
    
    context = dict(
        search_text = search_text,
        search_tag = tag,
        article_results = article_results,
        event_results = event_results,
        discussion_results = discussion_results,
        comment_results = comment_results,
        total_articles=total_articles,
        total_events=total_events,
        total_discussions=total_discussions,
        total_comments=total_comments,
        total = total_articles + total_events + total_discussions + total_comments,
        open_discussions=Discussion.objects.filter(is_open=True),
        other_contents=Article.objects.filter(published=True)[0:3],
        external_entries= RSSEntry.objects.exclude(date__gt=datetime.datetime.now()).order_by('-date','?')[:6],        
        search_section = search_section,
    )
    return context

@login_required
def search(request):    
    context = build_search_context(request, search_section='total')
    return render_to_response('search_results.html', context,
            context_instance=RequestContext(request))   
            
@login_required
def search_articles(request):    
    context = build_search_context(request, search_section='article')
    return render_to_response('search_article_results.html', context,
            context_instance=RequestContext(request))   
            
@login_required
def search_discussions(request):    
    context = build_search_context(request, search_section='discussion')
    return render_to_response('search_discussion_results.html', context,
            context_instance=RequestContext(request))   
            
@login_required
def search_events(request):    
    context = build_search_context(request, search_section='event')
    return render_to_response('search_event_results.html', context,
            context_instance=RequestContext(request))   
            
@login_required
def search_comments(request):    
    context = build_search_context(request, search_section='comment')
    return render_to_response('search_comment_results.html', context,
            context_instance=RequestContext(request))                                                   

@login_required
def search_tag(request, tag_slug):    
    tag = get_object_or_404(Tag, slug = tag_slug)
    context = build_search_context(request, search_section='total', tag=tag)
    return render_to_response('search_results.html', context,
            context_instance=RequestContext(request))       

@login_required
def search_articles_tag(request, tag_slug):   
    tag = get_object_or_404(Tag, slug = tag_slug)
    context = build_search_context(request, search_section='article', tag=tag)
    return render_to_response('search_article_results.html', context,
            context_instance=RequestContext(request))   
            
@login_required
def search_discussions_tag(request, tag_slug):   
    tag = get_object_or_404(Tag, slug = tag_slug)
    context = build_search_context(request, search_section='discussion', tag=tag)
    return render_to_response('search_discussion_results.html', context,
            context_instance=RequestContext(request))   
            
@login_required
def search_events_tag(request, tag_slug):    
    tag = get_object_or_404(Tag, slug = tag_slug)
    context = build_search_context(request, search_section='event', tag=tag)
    return render_to_response('search_event_results.html', context,
            context_instance=RequestContext(request))   
            
@login_required
def search_comments_tag(request, tag_slug):   
    tag = get_object_or_404(Tag, slug = tag_slug)
    context = build_search_context(request, search_section='comment', tag=tag)
    return render_to_response('search_comment_results.html', context,
            context_instance=RequestContext(request))             
            
#######################################################################
# Authentication
#######################################################################

def not_authenticated(func):
    """ decorator that redirect user to next page if
    he is already logged."""
    def decorated(request, *args, **kwargs):
        if request.user.is_authenticated():
            next = request.GET.get("next", "/")
            return HttpResponseRedirect(next)
        return func(request, *args, **kwargs)
    return decorated

def login_fw(request):
    login_form = FHAuthenticationForm()
    invitation_form = InvitationForm()
    if request.POST:
        login_form = FHAuthenticationForm(data=request.POST)
        
        if login_form.is_valid():
            if not request.POST.get('remember_me', None):
                request.session.set_expiry(0)
            login(request, login_form.get_user())
            return HttpResponseRedirect(reverse("home"))                 
            
    context = dict(
        login_form = login_form,
        invitation_form = invitation_form,
    )
        
    return render_to_response("login.html", context,
            context_instance=RequestContext(request))              

def logout_fw(request):
    logout(request)
    return HttpResponseRedirect(reverse("home")) 

def password_reset_fw(request):
    return password_reset(request, 
        template_name="password_reset.html",
        post_reset_redirect = reverse("password_reset_ok"),
        password_reset_form = FHPasswordResetForm,
        email_template_name = "email/reset_password.html"
    )

def password_reset_ok(request):
    context=dict()
    return render_to_response("password_reset_ok.html", context,
            context_instance=RequestContext(request))          

def password_reset_confirm_fw(request, uidb36, token):
    return password_reset_confirm(request, 
        uidb36=uidb36, 
        token=token, 
        template_name="password_reset_confirm.html",
        post_reset_redirect = reverse("password_reset_complete"),
        set_password_form=FHSetPasswordForm,
        )
    
def password_reset_complete_fw(request):
    return password_reset_complete(request, template_name="password_reset_complete.html")

#######################################################################
# Registration
#######################################################################
def register(request):   
    invitation_form = InvitationForm()
    login_form = FHAuthenticationForm()
    
    context = dict()
    if request.POST:
        invitation_form = InvitationForm(request.POST)
        if invitation_form.is_valid():
            code=invitation_form.cleaned_data.get('code','')
            invitation = Invitation.objects.filter(code = code)
            invitation = len(invitation) == 1 and invitation.get() or None 
            if invitation and not invitation.user:                
                request.session['register_code'] = code
                return HttpResponseRedirect(reverse('create-user'))        
            else:
                context.update(dict(used_code = True))
    
    context.update(dict(
        invitation_form = invitation_form,
        login_form = login_form
    ))
    return render_to_response("login.html", context,
                        context_instance=RequestContext(request))                 

def create_user(request):    
    code =  request.session.get('register_code', None)
    if not code:
        return HttpResponseRedirect(reverse('home'))        
    
    form = FHUserCreationForm()

    if request.POST:
        form = FHUserCreationForm(request.POST)
        if form.is_valid():    
            user = form.save()
            #Update invitation            
            invitation = Invitation.objects.get(code = code)
            invitation.user = user
            invitation.activation_date = datetime.datetime.now()
            invitation.save()

            del request.session['register_code']
            
            context = dict(user=user)

            #Send email to user
            template_obj = loader.get_template("email/wellcome_user.html")
            mail_context = Context(dict(user=user, BASE_HOST=settings.BASE_HOST))

            email_obj = EmailMessage(
              subject = _(u"Bienvenido"),
              body    = template_obj.render(mail_context),
              to      = [user.email],
              from_email = settings.DEFAULT_FROM_EMAIL,
            )
            email_obj.content_subtype = "html"
            email_obj.send()                         

            return render_to_response("wellcome.html", context,
                                context_instance=RequestContext(request))         
    context = dict(form=form)
    return render_to_response("create_user.html", context,
                        context_instance=RequestContext(request))                 

#######################################################################
# Newsletter
#######################################################################
@login_required()
def newsletter(request, slug):
    newsletter = get_object_or_404(Newsletter, slug=slug)
    context = dict(
        newsletter = newsletter,
    )
    return render_to_response("newsletter.html", context,
                        context_instance=RequestContext(request))        

@staff_member_required
def send_newsletter(request, slug):
    newsletter = get_object_or_404(Newsletter, slug=slug)
    for user in FHUser.objects.filter(newsletter=True):
        send_newsletter_to_email(user.email,newsletter)   
    
    newsletter.sent=True
    newsletter.save()
    
    return HttpResponseRedirect(settings.BASE_HOST+'/admin/familywealth/newsletter/')             


#######################################################################
# Inform
#######################################################################
@staff_member_required
def inform(request):
    context = dict(
        num_of_users = FHUser.objects.all().count(),
        num_of_valorations = Valoration.objects.all().count(),
        avg_valorations = Valoration.objects.aggregate(avg=Avg('value'))['avg'],
        num_of_comments = Comment.objects.filter(published=True).count(),
        num_of_articles = Article.objects.filter(published=True).count(),
        num_of_discussions = Article.objects.filter(published=True).count(),
        num_of_events = Event.objects.all().count(),
        total_confirmed_assistants = Assistant.objects.filter(status=1).count(),
        total_confirmed_not_assistants = Assistant.objects.filter(status=0).count(),
        total_favorited_content = Favourite.objects.all().count(),
        categories = Category.objects.all().count(),
        total_discussions_participants = Participant.objects.all().count(),
        total_newsletters_sended = Newsletter.objects.filter(sent=True).count(),
        total_newsletters = Newsletter.objects.all().count(),
        total_invitations = Invitation.objects.all().count(),
        unused_invitations = Invitation.objects.filter(user=None).count(),
        feedbak_notifications = FeedbackNotificacion.objects.all().count(),
    )
    return render_to_response("inform.html", context,
                        context_instance=RequestContext(request))                 


#######################################################################
# Informe ultimo login de cada usuario
#######################################################################
@staff_member_required
def last_login(request):
    l = []
    d0 = datetime.date(2013, 04, 23)
    d1 = datetime.date.today()
    delta = (d1 - d0).days
    for i in range(delta):
        l.append((d0 + datetime.timedelta(i + 1)).strftime("%d-%m-%Y"))

    context = dict(l = l, )

    return render_to_response("last_login.html", context,
            context_instance=RequestContext(request))

def download(request, filename):
    import subprocess
    data = subprocess.Popen(['cat', settings.REPORTS_PATH + '/' + filename], stdout=subprocess.PIPE).communicate()[0]
    response = HttpResponse(data, content_type='text/html')
    response['Content-Disposition'] = 'attachment; filename="' + filename + '"'

    return response
