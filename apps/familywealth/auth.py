from django.conf import settings
from django.contrib.auth.backends import ModelBackend
from django.core.exceptions import ImproperlyConfigured
from models import FHUser

class FHUserModelBackend(ModelBackend):
    def authenticate(self, username=None, password=None):
        try:
            users = FHUser.objects.filter(username=username)
        
            if users.count() == 0:
                user = FHUser.objects.get(email = username)
            else:
                user = users[0]
            
            if user.check_password(password):
                return user
        except FHUser.DoesNotExist:
            return None

    def get_user(self, user_id):
        try:
            return FHUser.objects.get(pk=user_id)
        except FHUser.DoesNotExist:
            return None

    @property
    def user_class(self):
        if not hasattr(self, '_user_class'):
            self._user_class = FHUser
            if not self._user_class:
                raise ImproperlyConfigured('Could not get custom user model')
        return self._user_class
