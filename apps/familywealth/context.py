from django.middleware import csrf
from django.conf import settings
def main(request):
    context = dict(
        csrf_simple_token = csrf.get_token(request),
        BASE_HOST = settings.BASE_HOST,
    )
    return context
