#!/usr/bin/python
# -*- coding: utf-8 -*-
from django import forms
from familywealth.models import FHUser, FeedbackNotificacion, REASON_CHOICES, Invitation, Comment, Article
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.forms import AuthenticationForm, PasswordResetForm, SetPasswordForm
from django.template.defaultfilters import filesizeformat
from django.conf import settings
from PIL import Image, ImageOps
import datetime


class FHUserAdminChangeForm(forms.ModelForm):
    email = forms.EmailField(label=_(u"E-mail"), required = True)
    class Meta:
        model = FHUser

class FHUserAdminCreateForm(forms.ModelForm):
    """
    A form that creates a user, with no privileges, from the given username and password.
    """
    username = forms.RegexField(label=_("Username"), max_length=30, regex=r'^[\w.@+-]+$',
        help_text = _("Required. 30 characters or fewer. Letters, digits and @/./+/-/_ only."),
        error_messages = {'invalid': _("This value may contain only letters, numbers and @/./+/-/_ characters.")})
    password1 = forms.CharField(label=_("Password"), widget=forms.PasswordInput)
    password2 = forms.CharField(label=_("Password confirmation"), widget=forms.PasswordInput,
        help_text = _("Enter the same password as above, for verification."))

    email = forms.EmailField(label=_(u"E-mail"), required = True)
    
    class Meta:
        model = FHUser
        #~ fields = ("username",)

    def clean_username(self):
        username = self.cleaned_data["username"]
        try:
            FHUser.objects.get(username=username)
        except FHUser.DoesNotExist:
            return username
        raise forms.ValidationError(_("Ya existe ese nombre de usuario."))

    def clean_password2(self):
        password1 = self.cleaned_data.get("password1", "")
        password2 = self.cleaned_data["password2"]
        if password1 != password2:
            raise forms.ValidationError(_("Las dos contraseñas no coinciden."))
        return password2
        
    def clean_email(self):
        email = self.cleaned_data["email"]
        try:
            FHUser.objects.get(email=email)
        except FHUser.DoesNotExist:
            return email
        raise forms.ValidationError(_(u"Ya existe un usuario con ese email."))       

    def save(self, commit=True):
        user = super(FHUserAdminCreateForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user

class FeedbackNotificacionForm(forms.ModelForm):
    reason = forms.ChoiceField(required=True, choices = REASON_CHOICES)        
    text = forms.CharField(required=True, max_length=500, widget = forms.Textarea(attrs = { 'rows' : '8', 'cols' : '30', 'class':'borracontenido'}))
    
    class Meta:
        model = FeedbackNotificacion
        
class InvitationForm(forms.Form):
    code = forms.CharField(max_length=300, widget=forms.TextInput(attrs={'required':'', 'class':'input_txt'}))

    def __init__(self, *args, **kwargs):
        super(InvitationForm, self).__init__(*args, **kwargs)

    def clean(self):
        cleaned_data = self.cleaned_data

        if "code" in cleaned_data:            
            if Invitation.objects.filter(code=cleaned_data['code']).count() != 1:
                self._errors["code"] = self.error_class([_(u'El código de activación no es válido')])
                raise forms.ValidationError(_(u'El código de activación no es válido'))

        return cleaned_data    
        
class CommentForm(forms.Form):
    text = forms.CharField(required=True, max_length=5000, widget = forms.Textarea(attrs = { 'rows' : '8', 'cols' : '30', 'class':'borracontenido'}))        
    parent_id = forms.CharField(required=False, max_length=5000, widget = forms.HiddenInput)    
    receive_notifications = forms.BooleanField(required=False)
    def __init__(self, *args, **kwargs):
        super(CommentForm, self).__init__(*args, **kwargs)

class ArticleAdminForm(forms.ModelForm):
    class Meta:
        model = Article

    def clean_file(self):
        if self.cleaned_data["type"]=='0':
            if "file" in self.cleaned_data and  self.cleaned_data["file"]: 
                return self.cleaned_data["file"]
            else:
                raise forms.ValidationError(_(u'Este campo es obligatorio.'))
        return self.cleaned_data["file"]
            
    def clean_link(self):
        if self.cleaned_data["type"]=='1':
            if "link" in self.cleaned_data and self.cleaned_data["link"]!='': 
                return self.cleaned_data["link"]
            else:
                raise forms.ValidationError(_(u'Este campo es obligatorio.'))
        return self.cleaned_data["link"]
            
    def clean_video(self):
        if self.cleaned_data["type"]=='2':
            if "video" in self.cleaned_data and self.cleaned_data["video"]!='': 
                return self.cleaned_data["video"]
            else:
                raise forms.ValidationError(_(u'Este campo es obligatorio.'))
        return self.cleaned_data["video"]

    def clean(self):
        cleaned_data = super(ArticleAdminForm, self).clean()
        if cleaned_data.get("publication")=='P' and not cleaned_data.get("publish_date"):
            raise forms.ValidationError(_(u'Ha seleccionado publicación programada. Por favor, rellene el campo "Publicado desde".'))
        elif cleaned_data.get("publication")=='P' and cleaned_data.get("publish_date") > datetime.datetime.now():
            cleaned_data["published"] = False
        elif cleaned_data.get("publication")=='P' and cleaned_data.get("publish_date") <= datetime.datetime.now():
            if not cleaned_data.get("unpublish_date"):
                cleaned_data["published"] = True
            elif cleaned_data.get("unpublish_date") > datetime.datetime.now():
                cleaned_data["published"] = True
            elif cleaned_data.get("unpublish_date") < datetime.datetime.now():
                cleaned_data["published"] = False
        return cleaned_data
            
#######################################################################
# Auth
#######################################################################
class FHAuthenticationForm(AuthenticationForm):
    username = forms.CharField(label=_(u"Username"), max_length=75, widget=forms.TextInput(attrs={'class':'input_txt'}))
    password = forms.CharField(label=_(u"Password"), widget=forms.PasswordInput(attrs={'class':'input_txt'}))

class FHPasswordResetForm(PasswordResetForm):
    email = forms.EmailField(label=_(u"E-mail"), max_length=75, widget=forms.TextInput(attrs={'class':'input_txt'}))

class FHSetPasswordForm(SetPasswordForm):
    """
    A form that lets a user change set his/her password without
    entering the old password
    """
    new_password1 = forms.CharField(label=_(u"New password"), widget=forms.PasswordInput(attrs={'class':'input_txt'}))
    new_password2 = forms.CharField(label=_(u"New password confirmation"), widget=forms.PasswordInput(attrs={'class':'input_txt'}))

class FHUserCreationForm(forms.ModelForm):
    """
    A form that creates a user, with no privileges, from the given username and password.
    """
    username = forms.RegexField(label=_(u"Username"), max_length=30, regex=r'^[\w.@+-]+$',
        help_text = _(u"Required. 30 characters or fewer. Alphanumeric characters only (letters, digits and underscores)."),
        error_message = _(u"This value must contain only letters, numbers and underscores."),
        widget=forms.TextInput(attrs={'class':'input_txt', 'tabindex':1}))
    password1 = forms.CharField(label=_(u"Password"), widget=forms.PasswordInput(attrs={'class':'input_txt', 'tabindex':3}))
    password2 = forms.CharField(label=_(u"Password confirmation"), widget=forms.PasswordInput(attrs={'class':'input_txt', 'tabindex':4}))
    email = forms.EmailField(label=_(u"E-mail"), max_length=75, widget=forms.TextInput(attrs={'class':'input_txt', 'tabindex':2}))
    accept_conditions = forms.BooleanField(required=True, widget=forms.CheckboxInput(attrs={'tabindex':5}))

    class Meta:
        model = FHUser
        fields = ("username","email")

    def clean_email(self):
        email = self.cleaned_data["email"]
        try:
            FHUser.objects.get(email=email)
        except FHUser.DoesNotExist:
            return email
        raise forms.ValidationError(_(u"Ya existe un usuario con ese email."))

    def clean_username(self):
        username = self.cleaned_data["username"]
        try:
            FHUser.objects.get(username=username)
        except FHUser.DoesNotExist:
            return username
        raise forms.ValidationError(_(u"Ya existe ese nombre de usuario."))

    def clean_password2(self):
        password1 = self.cleaned_data.get("password1", "")
        password2 = self.cleaned_data["password2"]
        if password1 != password2:
            raise forms.ValidationError(_(u"Las dos contraseñas no coinciden."))
        return password2

    def save(self, commit=True):
        user = super(FHUserCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user

class FHUserProfileForm(forms.ModelForm):
    """
    A form that creates a user, with no privileges, from the given username and password.
    """
    username = forms.RegexField(label=_(u"Username"), max_length=30, regex=r'^[\w.@+-]+$',
        help_text = _(u"Required. 30 characters or fewer. Alphanumeric characters only (letters, digits and underscores)."),
        error_message = _(u"This value must contain only letters, numbers and underscores."),
        widget=forms.TextInput(attrs={'class':'input_txt'}))
    password1 = forms.CharField(label=_(u"Password"), widget=forms.PasswordInput(attrs={'class':'input_txt'}), required=False)
    password2 = forms.CharField(label=_(u"Password confirmation"), widget=forms.PasswordInput(attrs={'class':'input_txt'}), required=False)
    email = forms.EmailField(label=_(u"E-mail"), max_length=75, widget=forms.TextInput(attrs={'class':'input_txt'}))
    
    position = forms.CharField(label=_(u"Position"), max_length=75, widget=forms.TextInput(attrs={'class':'input_txt'}), required=False)

    class Meta:
        model = FHUser
        fields = ("username","email", "position", "newsletter")

    def clean_email(self):
        email = self.cleaned_data["email"]
        
        if email==self.instance.email:
            return email
        else:
            try:
                FHUser.objects.get(email=email)
            except FHUser.DoesNotExist:
                return email
            raise forms.ValidationError(_(u"Ya existe un usuario con ese email."))

    def clean_username(self):
        username = self.cleaned_data["username"]

        if username==self.instance.username:
            return username
        else:
            try:
                FHUser.objects.get(username=username)
            except FHUser.DoesNotExist:
                return username
            raise forms.ValidationError(_(u"Ya existe ese nombre de usuario."))

    def clean_password2(self):
        #If password2 is not filled we dont change password
        if self.cleaned_data.get("password1", None) and self.cleaned_data.get("password2", None):
            password1 = self.cleaned_data.get("password1", "")
            password2 = self.cleaned_data["password2"]
            if password1 != password2:
                raise forms.ValidationError(_(u"Las dos contraseñas no coinciden."))
            return password2
        else:
            return ''

    def save(self, commit=True):
        user = super(FHUserProfileForm, self).save(commit=False)
        #If password2 is not filled we dont change password
        if self.cleaned_data.get("password1", None) and self.cleaned_data.get("password2", None):
            user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user

class FHUserAvatarForm(forms.ModelForm):
    image_url = forms.CharField(required=False, label=_(u"url"), max_length=300, widget=forms.TextInput(attrs={'class':'input_txt'}))
    image = forms.ImageField(required=False, widget=forms.FileInput())

    class Meta:
        model = FHUser
        fields = ("image_url","image",)
        
    def clean_image(self):
        image = self.cleaned_data.get("image", None)
        
        if image and hasattr(image,'content_type'):
            content_type = image.content_type.split('/')[0]

            if content_type in 'image':
                if image._size > settings.MAX_IMAGE_UPLOAD_SIZE:
                    raise forms.ValidationError(('El tamaño máximo es %s. El tamaño de su foto sin embargo es %s') % (filesizeformat(settings.MAX_IMAGE_UPLOAD_SIZE), filesizeformat(image._size)))
            else:
                raise forms.ValidationError(_('El tipo de fichero no está soportado'))

        return image

    def save(self, commit=True):
        user = super(FHUserAvatarForm, self).save(commit=True)
        if user.image:
            image = user.image
            file_path = image.path            
            
            #Generate image that fits exactly in ITEM_IMAGE_SIZE
            image = Image.open(file_path)                 
            # ImageOps compatible mode
            if image.mode not in ("L", "RGB"):
                image = image.convert("RGB")                            
            image.thumbnail((100,100), Image.ANTIALIAS)
            
            #Create white borders if necesary
            white_canvas = Image.new("RGB", (100,100), "white")
            x_paste = (white_canvas.size[0] - image.size[0])/2
            y_paste = (white_canvas.size[1] - image.size[1])/2
            white_canvas.paste(image, (x_paste, y_paste))                                
            white_canvas.save(file_path, 'JPEG', quality=75)

