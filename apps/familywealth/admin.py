#!/usr/bin/python
# -*- coding: utf-8 -*-
from django.contrib import admin
from django.contrib.auth.models import User, Group
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.forms import UserChangeForm
from django.utils.translation import ugettext, ugettext_lazy as _
from familywealth.forms import FHUserAdminChangeForm, FHUserAdminCreateForm, ArticleAdminForm
from familywealth.models import *
from django.template import loader, Context
from django.core.mail import EmailMessage
from django.conf import settings
from tagging.models import TaggedItem, Tag
from django_authopenid.models import UserAssociation
import datetime

class CommentAdmin(admin.ModelAdmin):
    search_fields = ['user__email', 'user__username', 'text']
admin.site.register(Comment, CommentAdmin)

class ValorationAdmin(admin.ModelAdmin):
    search_fields = ['user__email', 'user__username', 'user__first_name', 'user__last_name', 'content__title', 'content__short_description', 'content__long_description']
admin.site.register(Valoration, ValorationAdmin)

class FavouriteAdmin(admin.ModelAdmin):
    search_fields = ['user__email', 'user__username', 'user__first_name', 'user__last_name', 'content__title', 'content__short_description', 'content__long_description']
admin.site.register(Favourite, FavouriteAdmin)

class NewsletterAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug' : ['title']}
    list_filter = ('sent',)
    list_display = ('__unicode__', 'preview', 'show_date')    

    readonly_fields=('show_date',)

    def test(self, request, queryset):
        emails = []
        for newsletter in queryset:
            for u in FHUser.objects.filter(is_staff=True):
                emails.append(u.email)
                newsletter.send(u.email, request)            
        self.message_user(request, u"La newsletter de prueba fue enviada a: %s" % (','.join(emails)))
        
    test.short_description = u"Probar newsletter (solamente la recibirán los miembros del staff)"

    def send(self, request, queryset):
        emails = []
        for newsletter in queryset:
            for user in FHUser.objects.filter(newsletter=True):
                emails.append(user.email)
                newsletter.send(user.email, request)            
            newsletter.sent = True
            newsletter.date = datetime.datetime.now()
            newsletter.save()
        self.message_user(request, "La newsletter fue enviada a: %s" % (','.join(emails)))       
        
        
    send.short_description = u"Enviar newsletter (a todos los usuarios que tengan la opción confirmada)"

    actions = [test, send]
    
    search_fields = ['title', 'description']

admin.site.register(Newsletter, NewsletterAdmin)

class InvitationAdmin(admin.ModelAdmin):
    readonly_fields=('usuario','fecha_de_activacion',)
    list_display = ('__unicode__', 'email', 'fecha_de_activacion', 'friendly_user')        
    search_fields = ['user__email', 'user__username', 'code', 'email']

admin.site.register(Invitation, InvitationAdmin)

class NotificationAdmin(admin.ModelAdmin):
    search_fields = ['user__email', 'user__username', 'user__first_name', 'user__last_name', 'content__title', 'content__short_description', 'content__long_description']

admin.site.register(Notification, NotificationAdmin)

#Rss entries inline
class RSSEntryInline(admin.StackedInline):
    model = RSSEntry
    extra=1
    
class RSSSourceAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug' : ['title']}
    inlines = [
        RSSEntryInline,
    ]    
    
    search_fields = ['title', 'source', 'short_description', 'long_description']
    
admin.site.register(RSSSource, RSSSourceAdmin)

class FeedbackNotificacionAdmin(admin.ModelAdmin):
    list_display = ('__unicode__', 'user', 'answered')    
    list_filter = ('answered',)
    
    search_fields = ['user__email', 'user__username', 'user__first_name', 'reason', 'text']
    
admin.site.register(FeedbackNotificacion, FeedbackNotificacionAdmin)

#Comments inline
class CommentInline(admin.StackedInline):
    model = Comment
    extra=1

class ArticleAdmin(admin.ModelAdmin):
    form = ArticleAdminForm

    prepopulated_fields = {'slug' : ['title']}
    readonly_fields=('creado',)
    search_fields = ['title','short_description', 'long_description']
    radio_fields = {'publication': admin.HORIZONTAL}
    list_display = ('__unicode__', 'creation_date', 'category')    

    inlines = [
        CommentInline,
    ]
    class Media:
        js = ("/static/js/publication.js", )

    
admin.site.register(Article, ArticleAdmin)

#Articles inline in categories
class ArticleInline(admin.StackedInline):
    model = Article
    extra=1

class CategoryAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug' : ['title']}
    
    inlines = [
        ArticleInline,
    ]
    
    search_fields = ['title','description']

admin.site.register(Category, CategoryAdmin)

#Participants inline in discussions
class ParticipantInline(admin.StackedInline):
    model = Participant
    extra=1

class DiscussionAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug' : ['title']}
    readonly_fields=('creado',)
    list_display = ('__unicode__', 'creation_date', 'is_open')        
    inlines = [
        ParticipantInline,
        CommentInline,
    ]
    
    search_fields = ['title','short_description', 'long_description']

admin.site.register(Discussion, DiscussionAdmin)

#Assistans inline in events
class AssistantInline(admin.StackedInline):
    model = Assistant
    extra=1     
    
#Links inline in events
class EventLinktInline(admin.StackedInline):
    model = EventLink
    extra=1

#Files inline in events
class EventFileInline(admin.StackedInline):
    model = EventFile
    extra=1

#Images inline in events
class EventImagetInline(admin.StackedInline):
    model = EventImage
    extra=1

class EventAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug' : ['title']}
    readonly_fields=('creado',)
    list_display = ('__unicode__', 'date',)    
    inlines = [
        EventLinktInline,
        EventFileInline,
        EventImagetInline,
        AssistantInline,
        CommentInline,
    ]
    
    search_fields = ['title','short_description', 'long_description']

admin.site.register(Event, EventAdmin)

#Favourites inline
class FavouriteInline(admin.StackedInline):
    model = Favourite
    extra=1
    
#Valorations inline
class ValorationInline(admin.StackedInline):
    model = Valoration
    extra=1    

#Valorations inline
class InvitationInline(admin.StackedInline):
    model = Invitation
    extra=0
    max_num=1

#FH users instead of standard users
admin.site.unregister(User)
admin.site.unregister(Group)

class FHUserAdmin(UserAdmin):

    change_form_template = 'admin_fh_user_change_form.html'

    form = FHUserAdminChangeForm   
    add_form = FHUserAdminCreateForm   
    
    fieldsets = (
        (None, {'fields': ('username', 'password')}),
        (_('Personal info'), {'fields': ('first_name', 'last_name', 'email', 'position', 'image', 'image_url', 'newsletter', 'is_bbva_user', 'is_iese_user')}),
        #(_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser', 'user_permissions')}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser')}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
        #(_('Groups'), {'fields': ('groups',)}),
    )    

    add_fieldsets = (
        (None, {'fields': ('username', 'password1', 'password2')}),
        (_('Personal info'), {'fields': ('first_name', 'last_name', 'email', 'position', 'image', 'image_url', 'newsletter', 'is_bbva_user', 'is_iese_user')}),
        #(_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser', 'user_permissions')}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser')}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
        #(_('Groups'), {'fields': ('groups',)}),
    )

    list_display = ('username', 'short_email', 'short_position', 'short_name', 'last_name', 'is_staff', 'short_newsletter', 'short_is_bbva_user', 'short_is_iese_user', 'is_active')

    inlines = [
        InvitationInline,
        CommentInline,
        FavouriteInline,
        ValorationInline,
    ]    

admin.site.register(FHUser, FHUserAdmin)

#Unregistering tags
admin.site.unregister(TaggedItem)
admin.site.unregister(Tag)

#Unregistering authopenid
admin.site.unregister(UserAssociation)
