# -*- coding: utf-8 -*- 
from django import template
from django.template import RequestContext, loader, Context
from django.conf import settings
from django.core.urlresolvers import reverse
from django.utils.translation import ungettext, ugettext as _
import datetime
from familywealth.models import Article, Discussion, Event, Content, Category

def show_friendly_name(user):
    """
        Returns a friendly name for the user depending on the filled attributes
    """
    name = ''
    if not user.is_anonymous() and user.first_name:
        name += user.first_name
        if user.last_name:
            name += " " + user.last_name   
    else:
        name = user.username
    return name

def show_friendly_avatar(user):
    """
        Returns an avatar url. If the user hasn't got avatar returns default image
    """
    name = ''
    if not user.is_anonymous():
        if user.image_url:
            return user.image_url
        elif user.image:
            return settings.BASE_HOST + user.image.url
        else:
            return settings.BASE_HOST + settings.MEDIA_URL + 'img/default_avatar.jpg'
    else:
        return settings.BASE_HOST + settings.MEDIA_URL + 'img/default_avatar.jpg'

def show_friendly_content_image(content):
    """
        Returns a content image url. If the article hasn't got one returns default image
    """
    name = ''
    if content.image:
        return content.image.url
    else:

        if content.__class__ == Article:
            return settings.MEDIA_URL + 'img/default_article.jpg'
        
        elif content.__class__ == Discussion:
            return settings.MEDIA_URL + 'img/default_discussion.jpg'

        elif content.__class__ == Event:
            return settings.MEDIA_URL + 'img/default_event.jpg'

def show_friendly_content_miniature(content):
    """
        Returns a content image url. If the article hasn't got one returns default image
    """
    name = ''
    if content.image:
        return content.image.url
    else:
        return settings.MEDIA_URL + 'img/default_miniature.jpg'
def extra_comment_class(number):
    """
        Calculates the extra class for comments
    """
    mod = number%3
    if mod == 1:
        return ""
    elif mod == 2:
        return "comentario_azul"
    elif mod == 0:
        return "comentario_rosa"
    
    return ""
    
def url_for_type(instance):
    
    if instance.__class__ == Article:
        if instance.type=='0':
            return reverse('content-pdf', args=[instance.slug])
        elif instance.type=='1':
            return reverse('content-link', args=[instance.slug])
        elif instance.type=='2':
            return reverse('content-video', args=[instance.slug])
        else:
            return reverse('content', args=[instance.slug])
    
    elif instance.__class__ == Discussion:
        return reverse('discussion', args=[instance.slug])

    elif instance.__class__ == Event:
        return reverse('event', args=[instance.slug])
    
    elif instance.__class__ == Content:
        article = Article.objects.filter(pk = instance.pk)
        discussion = Discussion.objects.filter(pk = instance.pk)
        event = Event.objects.filter(pk = instance.pk)
        if article:
            return url_for_type(article.get())
        elif discussion:
            return url_for_type(discussion.get())
        elif event:
            return url_for_type(event.get())    


def date_diff(d):
    #human readable date diff.

    now = datetime.datetime.now()
    today = datetime.datetime(now.year, now.month, now.day)
    delta = now - d
    delta_midnight = today - d
    days = delta.days
    hours = round(delta.seconds / 3600., 0)
    minutes = round(delta.seconds / 60., 0)
    chunks = (
        (365.0, lambda n: ungettext('año', 'años', n)),
        (30.0, lambda n: ungettext('mes', 'meses', n)),
        (7.0, lambda n : ungettext('semana', 'semanas', n)),
        (1.0, lambda n : ungettext('día', 'días', n)),
    )
    
    if days == 0:
        if hours == 0:
            if minutes > 0:
                return ungettext('hace un minuto', \
                    'hace %(minutes)d minutos', minutes) % \
                    {'minutes': minutes}
            else:
                return _(u"hace menos de un minuto")
        else:
            return ungettext('hace una hora', 'hace %(hours)d horas', hours) \
            % {'hours':hours}

    if delta_midnight.days == 0:
        return _(u"ayer a las %s") % d.strftime("%H:%M")

    count = 0
    for i, (chunk, name) in enumerate(chunks):
        if days >= chunk:
            count = round((delta_midnight.days + 1)/chunk, 0)
            break

    return _(' hace %(number)d %(type)s') % \
        {'number': count, 'type': name(count)}

#Menu categories
class CageroriesMenu(template.Node):
    def render(self, context):
        cats = Category.objects.all()
        cats_len = cats.count()
        cats_1=[]
        cats_2=[]
        cats_3=[]
        for i in range(len(cats)):
            cat = cats[i]
            if i%3 == 0:
                cats_1.append(cat)
            elif i%3 == 1:
                cats_2.append(cat)
            else:
                cats_3.append(cat)
        context['categories_1'] = cats_1
        context['categories_2'] = cats_2
        context['categories_3'] = cats_3
        return loader.render_to_string('portlet_categories.html', context)

def display_categories_menu(parser, token):
    return CageroriesMenu()
    
class GenericSearchMenu(template.Node):
    """ Generic search menu """
    def render(self, context):
        return loader.render_to_string('iou/helpers/profile/generic.menu.html', context)
    

register = template.Library()
register.filter('friendly_username', show_friendly_name)
register.filter('extra_comment_class', extra_comment_class)
register.filter('friendly_avatar', show_friendly_avatar)
register.filter('friendly_content_image', show_friendly_content_image)
register.filter('friendly_content_miniature', show_friendly_content_miniature)
register.filter('url_for_type', url_for_type)
register.filter('date_diff', date_diff)
register.tag('display_categories_menu', display_categories_menu)


