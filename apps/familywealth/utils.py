import sys

def sys_error_log(text):
    """ Method for log errors with apache-log or syslog. """
    sys.stderr.write("\n*** FAMILY WEALTH LOG:")
    sys.stderr.write(unicode(text).encode('utf-8'))
    sys.stderr.write("\n")
    sys.stderr.flush()
