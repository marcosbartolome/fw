#!/usr/bin/python
# -*- coding: utf-8 -*-

import os
import sys
os.environ['DJANGO_SETTINGS_MODULE'] = 'settings'   #To be able to import this Django project's "settings" module
sys.path.append(os.path.join(os.path.dirname(os.path.abspath(__file__)),'../'))
from django.contrib.webdesign import lorem_ipsum
import datetime as dt
import random
from django.core.files import File
import settings
from familywealth.models import *
from django.contrib.sites.models import Site
from django.db.utils import IntegrityError
from django.db import transaction

MAX_USERS=30
MAX_CATEGORIES=6
MAX_ARTICLES=30
MAX_COMMENTS_PER_ITEM=8
MAX_DISCUSSIONS=10
MAX_PARTICIPANTS_PER_DISCUSSION=20
MAX_EVENTS=10
MAX_ASSISTANTS_PER_EVENT=20
MAX_FAVOURITES_PER_USER=20
MAX_VALORATIONS_PER_USER=20
MAX_ASSISTANCES_PER_USER=10
MAX_DISCUSSIONS_PER_USER=10
MAX_NOTIFICATIONS_PER_USER=10
MAX_IMAGES_PER_EVENT=5
MAX_LINKS_PER_EVENT=5
MAX_FILES_PER_EVENT=5

def random_word():
    """Random text with 1 word."""
    return lorem_ipsum.words(1, common=False)
    
def random_email():
    """Random mail address."""
    return lorem_ipsum.words(1, common=False) + u'@' + lorem_ipsum.words(1, common=False) + \
           random.choice([u'.es', u'.com', u'.org', u'.net', u'.gov', u'.tk'])

def random_value(max_value):
    """Random number from 0 to max_value - 1."""
    return random.randrange(0, max_value)

def random_sentence():
    """Random text with variable number of words, one sentence."""
    return lorem_ipsum.sentence()

def random_paragraph():
    """Random text with variable number of words, several sentences."""
    return lorem_ipsum.paragraph()    

def random_number(ndigits):
    """Random number from 0 to 999[with the given number of digits]."""
    return random.randrange(0, 10 ** ndigits)

def random_number_string(ndigits):
    """Random number from 0 to 999[with the given number of digits], in string format, filled by 0s on the left."""
    return u''.join(random.choice(u'0123456789') for i in range(ndigits))

def random_name():
    """Random text with 5 words."""
    return lorem_ipsum.words(5, common=False)

def random_boolean():
    return random.randrange(0, 2) == 0

def random_image(width, height):
    import Image
    im = Image.new("RGB", (width, height))
    import ImageDraw, ImageFont
    ttf = ImageFont.truetype("static/sample/sample.ttf", 80)
    draw = ImageDraw.Draw(im)
    draw.rectangle([0,0,width,height], fill= "rgb(%d,%d,%d)"%(1+random_value(255),1+random_value(255),1+random_value(255)))
    draw.text((0, 0), random_word(), font=ttf)
    im.rotate(45)
    return im

def random_future_date():
    """Random date between today and today + one year - one day."""
    return dt.date.today() + dt.timedelta(random.randrange(0, 365))

def random_past_date():
    """Random date between today and today + one year - one day."""
    return dt.date.today() - dt.timedelta(random.randrange(0, 365))

def create_users():
    for user_num in range(0, MAX_USERS):
        print "User: %s of %s" %(str(user_num),str(MAX_USERS))
        image = random_image(100,100).save("avatar.jpg", "JPEG")
        user = FHUser(
            username = random_word() + str(user_num),
            first_name = random_word(),
            last_name = random_word(),
            email = random_email(),
            is_active = True,
            is_staff = False,
            is_superuser = False,
            position = random_word(),            
            image = File(open('avatar.jpg', 'r')),
            is_bbva_user = random_boolean() and random_boolean() and random_boolean(),
            is_iese_user = random_boolean() and random_boolean() and random_boolean(),
        )
        user.set_password(u'wedd')
        user.save()

def create_categories():
    for i in range(MAX_CATEGORIES):
        print "Category: %s of %s" %(str(i),str(MAX_CATEGORIES))
        category = Category(
            title = random_name(),
            description = (random_word() + " " )* random_value(8),   
        )
        category.save()

def create_articles():   
    for i in range(MAX_ARTICLES):
        print "Article: %s of %s" %(str(i),str(MAX_ARTICLES))
        type = str(random_value(3))

        image = random_image(247,157).save("image.jpg", "JPEG")
        miniature = random_image(100,100).save("miniature.jpg", "JPEG")

        if i%2==0:
            date = random_future_date()
        else:
            date = random_past_date()
        
        article = Article(
            category = Category.objects.order_by('?')[0],
            type = type,
            title = random_name(),
            short_description = random_sentence(),
            long_description = random_paragraph(),       
            creator = FHUser.objects.order_by('?')[0],
            image = File(open('image.jpg', 'r')),
            miniature = File(open('miniature.jpg', 'r')),
            tags = random_word() + u',' + random_word() + u',' + random_word(),
            creation_date=date,
        )
        
        if type=='0':
            article.file =  File(open('static/sample/test.pdf', 'r'))
        elif type=='1':
            article.link = 'http://'+random_word()
        else:
            article.video = 'hYTz1hElBaw'
        
        article.save()

        article.creation_date = date
        article.save()
        
        for i in range(random_value(MAX_COMMENTS_PER_ITEM)):
            print "  Comment: %s of %s" %(str(i),str(MAX_COMMENTS_PER_ITEM))
            comment = Comment(
                content = Article.objects.order_by('?')[0],
                user = FHUser.objects.order_by('?')[0],
                text = random_paragraph(),  
                parent_comment = None,
            )
            comment.save()

def create_discussions():
    for i in range(MAX_DISCUSSIONS):
        
        print "Discussion: %s of %s" %(str(i),str(MAX_DISCUSSIONS))

        image = random_image(157,157).save("image.jpg", "JPEG")
        miniature = random_image(100,100).save("miniature.jpg", "JPEG")

        if i%2==0:
            date = random_future_date()
        else:
            date = random_past_date()
        
        if i==0:
            title = "Debate libre"
        else:
            title = random_name()
        
        discussion=Discussion(
            is_open=Discussion.objects.filter(is_open=True).count() < 4,
            title = title,
            short_description = random_sentence(),
            long_description = random_paragraph(),       
            creator = FHUser.objects.order_by('?')[0],
            image = File(open('image.jpg', 'r')),
            miniature = File(open('miniature.jpg', 'r')),
            tags = random_word() + u',' + random_word() + u',' + random_word(),            
            creation_date = date,
        )
        discussion.save()
        
        discussion.creation_date = date
        discussion.save()

        users = FHUser.objects.order_by('?')[0:random_value(MAX_PARTICIPANTS_PER_DISCUSSION)]

        for i in range(len(users)):
            print "  Participant: %s of %s" %(str(i),str(len(users)))
            participant = Participant(
                user=users[i],
                discussion=discussion,
            )
            participant.save()

            comment = Comment(
                content = discussion,
                user = users[i],
                text = random_paragraph(),  
                parent_comment = None,
            )
            comment.save()            

def create_events():
    for i in range(MAX_EVENTS):
        
        print "Event: %s of %s" %(str(i),str(MAX_EVENTS))

        
        image = random_image(157,157).save("image.jpg", "JPEG")
        miniature = random_image(100,100).save("miniature.jpg", "JPEG")
        
        if i%2==0:
            date = random_future_date()
        else:
            date = random_past_date()
        
        event=Event(
            date = date, 
            location = random_word() + u',' + random_word() + u',' + random_word(),                      
            title = random_name(),
            short_description = random_sentence(),
            long_description = random_paragraph(),       
            creator = FHUser.objects.order_by('?')[0],
            image = File(open('image.jpg', 'r')),
            miniature = File(open('miniature.jpg', 'r')),
            planning = random_paragraph()+ random_paragraph() + random_paragraph(),       
            tags = random_word() + u',' + random_word() + u',' + random_word(),            
        )
        event.save()

        users = FHUser.objects.order_by('?')[0:random_value(MAX_ASSISTANTS_PER_EVENT)]

        for i in range(len(users)):
            print "  Assistant: %s of %s" %(str(i),str(len(users)))
            assistant = Assistant(
                user=users[i],
                event=event,
            )
            assistant.save()

        for i in range(random_value(MAX_COMMENTS_PER_ITEM)):
            print "  Comment: %s of %s" %(str(i),str(MAX_COMMENTS_PER_ITEM))
            comment = Comment(
                content = Event.objects.order_by('?')[0],
                user = FHUser.objects.order_by('?')[0],
                text = random_paragraph(),  
                parent_comment = None,
            )
            comment.save()
            
        for i in range(random_value(MAX_IMAGES_PER_EVENT)):
            print "  EventImage: %s of %s" %(str(i),str(MAX_IMAGES_PER_EVENT))

            image = random_image(500,500).save("image.jpg", "JPEG")
            miniature = random_image(100,100).save("miniature.jpg", "JPEG")            
            
            event_image = EventImage(
                event = event,
                title = random_sentence()[0:255],
                description = random_paragraph()[0:499],    
                image = File(open('image.jpg', 'r')),
                miniature = File(open('miniature.jpg', 'r')),
            )
            event_image.save()            

        for i in range(random_value(MAX_LINKS_PER_EVENT)):
            print "  EventLink: %s of %s" %(str(i),str(MAX_LINKS_PER_EVENT))
            
            event_link = EventLink(
                event = event,
                title = random_sentence()[0:255],
                description = random_paragraph()[0:499],    
                link = "http://"+random_word(),  
            )
            event_link.save()        
            
        for i in range(random_value(MAX_FILES_PER_EVENT)):
            print "  EventFile: %s of %s" %(str(i),str(MAX_FILES_PER_EVENT))
            
            event_file = EventFile(
                event = event,
                title = random_sentence()[0:255],
                description = random_paragraph()[0:499],    
                file =  File(open('static/sample/test.pdf', 'r')),
            )
            event_file.save()                   

def create_sources():
    
    urls = ["http://rss.news.yahoo.com/rss/business", 
            "http://feeds.bbci.co.uk/news/rss.xml", 
            "http://feeds.reuters.com/reuters/GCAeconomicNews"]
    for i in range(len(urls)):
        print "Source: %s of %s" %(str(i),str(len(urls)))
        
        image =  random_image(100,100).save("miniature.jpg", "JPEG")
        source = RSSSource(
            title = random_name(),
            source = urls[i],   
            short_description = random_sentence(),
            long_description = random_paragraph(),
            image = File(open('miniature.jpg', 'r')),      
        )
        source.save()

@transaction.commit_manually
def update_users():    
    users = FHUser.objects.all()
    for u in range(users.count()):
        user = users[u]
        
        print "Creating favourites for user %s of %s"%(str(u),users.count())
        for i in range(MAX_FAVOURITES_PER_USER):
            print "  Favourite: %s of %s for user %s" %(str(i),str(MAX_FAVOURITES_PER_USER),str(user))
            sid = transaction.savepoint()
            try:
                fav = Favourite(
                    content = Content.objects.order_by('?')[0],
                    user = user,
                )                
                fav.save()
            except IntegrityError, e:
                transaction.rollback()
            else:
                transaction.commit()

        print "Creating valorations for user %s of %s"%(str(u),users.count())
        for i in range(MAX_VALORATIONS_PER_USER):
            print "  Valoration: %s of %s for user %s" %(str(i),str(MAX_FAVOURITES_PER_USER),str(user))
            sid = transaction.savepoint()
            try:
                obj = Valoration(
                    content = Content.objects.order_by('?')[0],
                    user = user,
                    value = 1+ random_value(5),
                )                
                obj.save()
            except IntegrityError, e:
                transaction.rollback()
            else:
                transaction.commit()

        print "Creating assistance to events for user %s of %s"%(str(u),users.count())
        for i in range(MAX_ASSISTANCES_PER_USER):
            print "  Assistance: %s of %s for user %s" %(str(i),str(MAX_FAVOURITES_PER_USER),str(user))
            sid = transaction.savepoint()
            try:
                obj = Assistant(
                    event = Event.objects.order_by('?')[0],
                    user = user,
                    status = random_value(2),
                )                
                obj.save()
            except IntegrityError, e:
                transaction.rollback()
            else:
                transaction.commit()
                
        print "Creating participation in discussions for user %s of %s"%(str(u),users.count())
        for i in range(MAX_DISCUSSIONS_PER_USER):
            print "  Participation: %s of %s for user %s" %(str(i),str(MAX_FAVOURITES_PER_USER),str(user))
            sid = transaction.savepoint()
            try:
                obj = Participant(
                    discussion = Discussion.objects.order_by('?')[0],
                    user = user,
                )                
                obj.save()
            except IntegrityError, e:
                transaction.rollback()
            else:
                transaction.commit()                
                
        print "Creating notifications for user %s of %s"%(str(u),users.count())
        for i in range(MAX_NOTIFICATIONS_PER_USER):
            print "  Notification: %s of %s for user %s" %(str(i),str(MAX_FAVOURITES_PER_USER),str(user))
            sid = transaction.savepoint()
            try:
                obj = Notification(
                    content = Content.objects.order_by('?')[0],
                    user = user,
                )                
                obj.save()
            except IntegrityError, e:
                transaction.rollback()
            else:
                transaction.commit()                

create_users()
create_categories()
create_articles()
create_discussions()
create_events()
create_sources()
update_users()
