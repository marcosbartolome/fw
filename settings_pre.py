
from settings_common import *

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql', # Add 'postgresql_psycopg2', 'postgresql', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': 'fwealth',                      # Or path to database file if using sqlite3.
        'USER': 'fwealth',                      # Not used with sqlite3.
        'PASSWORD': 'Bbujh4QE',                  # Not used with sqlite3.
        'HOST': 'localhost',                      # Set to empty string for localhost. Not used with sqlite3.
        'PORT': '3306',                      # Set to empty string for default. Not used with sqlite3.
    }
}

INSTALLED_APPS = (
    'django.contrib.webdesign',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.admin',
    'south',
    'mysqlfulltextsearch',
    'bbvafamilywealth.apps.tagging',
    'django_authopenid',  
    'familywealth',      
)


